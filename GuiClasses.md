classDiagram

    class Content {
    }

    class Button {
    }

    class Label {
    }

    class Image {
        +string path
        +init()
    }
    
    Content <|-- Image
    Content <|-- Label
    Content <|-- Button
