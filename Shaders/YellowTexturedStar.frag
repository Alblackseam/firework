#version 400 core

uniform sampler2D diffuseTexture;


in WireframeVertex {
	vec2 texCoord;
} fs_in;


out vec4 fragColor;


void main()
{
	vec4 diffuseTextureColor = texture( diffuseTexture, fs_in.texCoord ).rgba;
	fragColor = vec4(1,1,0,1) * 10.0f * diffuseTextureColor.a;
}
