#version 430 core

struct ParticleVertexData
{
	vec4	position;
	vec4	color;
	vec4	uv;
};

layout (location = 0) in ParticleVertexData particleVertex;

uniform mat4 viewMatrix, projMatrix;

out vec4 color;
out vec2 texCoord;

void main()
{
	color = particleVertex.color;
	texCoord = particleVertex.uv.xy;
	//gl_Position = projMatrix * viewMatrix * particleVertex.position;
	gl_Position = particleVertex.position;
}
