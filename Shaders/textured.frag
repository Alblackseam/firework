#version 430 core 

uniform sampler2D ourTexture;
uniform vec4 color;

in	vec2 texCoord;
out	vec4 Out;

void main()
{
	vec4 texColor = texture(ourTexture, texCoord);
	
	Out = mix(texColor, vec4(color.x, color.y, color.z, 1.0f), color.w);
	//Out = vec4(color.x, color.y, color.z, 1.0f);
}
