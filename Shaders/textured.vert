#version 430 core

layout (location = 0) in vec3 position;

uniform mat4 projMatrix;
uniform mat4 viewMatrix;

out vec2 texCoord;

void main()
{
	texCoord = vec2(0.5 + 0.5 * position.x, 0.5 - 0.5 * position.y);
	gl_Position = projMatrix * viewMatrix * vec4(position, 1);
}
