#version 430 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoordIn;

uniform mat4 projMatrix;
uniform mat4 viewMatrix;

out vec2 texCoord;

void main()
{
	texCoord = texCoordIn;
	gl_Position = projMatrix * viewMatrix * vec4(position, 1);
}
