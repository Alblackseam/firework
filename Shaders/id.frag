#version 430 core 

uniform sampler2D ourTexture;

in vec2 texCoord;
in float objectId;
out	vec4 Out;

void main()
{
	//Out = texture(ourTexture, texCoord);
	Out = vec4(objectId, 0, 0, 1);
}
