#version 430 core

layout (location = 0) in vec3 position;

uniform mat4 viewMatrix, projMatrix;
uniform int objId;

out vec2 texCoord;
out float objectId;

void main()
{
	texCoord = vec2(0.5 + 0.5 * position.x, 0.5 - 0.5 * position.y);
	objectId = objId * 1.0 / 256;
	gl_Position = projMatrix * viewMatrix * vec4(position, 1);
}
