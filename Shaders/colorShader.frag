#version 430 core 

uniform sampler2D ourTexture;

in	vec4 color;
in	vec2 texCoord;
out	vec4 Out;

void main()
{
	Out = texture(ourTexture, texCoord) * color;
}
