#version 150 core

in vec3 vertexPosition;
in vec3 vertexNormal;
in mat4 bulletPlace;

out vec3 position;
out vec3 normal;

uniform mat4 modelView;
uniform mat3 modelViewNormal;
uniform mat4 mvp;

void main()
{
	//vec4 vertexNormalTransformed = bulletPlace * vec4(vertexNormal, 1.0);

	vec3 vp = vec3(vertexPosition.x * 0.1, vertexPosition.z * 0.1, -vertexPosition.y * 0.3);
	vec3 vn = vec3(vertexNormal.x, vertexNormal.z, -vertexNormal.y);

	mat3 bulletPlace3 = mat3(bulletPlace[0][0], bulletPlace[0][1], bulletPlace[0][2],
							 bulletPlace[1][0], bulletPlace[1][1], bulletPlace[1][2],
							 bulletPlace[2][0], bulletPlace[2][1], bulletPlace[2][2]
	);

	normal = normalize( modelViewNormal * bulletPlace3 * vn );
	position = vec3( modelView * bulletPlace * vec4( vp, 1.0 ) );

	gl_Position = mvp * bulletPlace * vec4( vp, 1.0 );
}
