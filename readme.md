Test

```mermaid
classDiagram

    class Content {
    }

    class Button {
    }

    class Label {
    }

    class Image {
        +string path
        +init()
    }

    class Origin {
    }

    class OriginRect {
    }

    Content <|-- Image
    Content <|-- Label
    Content <|-- Button
```

huhuh


```mermaid
classDiagram
    class Place {
      cells : array~int,3~
      type : enum // Village or Town
    }

    class Road {
      cells : array~int,2~
    }


    class PlayerState {
      playerName : string
    }

    class GameState {
    }

    class Resources {
      count : array~int, 5~
    }

    Place <-- PlayerState : places
    Road <-- PlayerState : roads
    Resources <-- PlayerState : resources
    PlayerState "1..*" <--* GameState : players
 
    class Player {
      name : string
    }

    GameState "1..*" <--* Player : gameStates
```
