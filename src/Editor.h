#pragma once
#include "Math/Vector.h"

#include "Game/Scene.h"


struct Editor // SceneController
{
	Editor(Scene& scene_, std::string jsonPath = "Editor.json");
	void init();

	Scene& scene;

	void render(col::Graphics& graphics);

protected:
	int currentTextureIndex = 0;

	void onHexCenterClicked(const std::vector<HexDistanceInfo>&);
	void onHexEdgeClicked(const std::vector<HexDistanceInfo>&);
	void onHexCrossClicked(const std::vector<HexDistanceInfo>&);

	void parse();
};


