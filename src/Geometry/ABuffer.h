#pragma once

#include <algorithm>

#include "Common.h"


struct ABuffer
{
	GLuint bufferId{};
	GLenum target;
	GLenum usage;
	GLuint vertexCount{};

	ABuffer(GLenum target, GLenum usage)
		: target{target}
		, usage{usage}
	{
		glGenBuffers(1, &bufferId);
	}

	~ABuffer()
	{
		if (bufferId)
			glDeleteBuffers(1, &bufferId);
	}

	template<typename Vertex>
	void setData(const std::vector<Vertex>& vertices)
	{
		vertexCount = vertices.size();
		glBindBuffer(target, bufferId);
		glBufferData(target, vertexCount * sizeof(Vertex), vertices.data(), usage);
	}

	void bind()
	{
		glBindBuffer(target, bufferId);
	}

	template<typename Struct>
	void setDataStructures(const std::vector<Struct>& vertices)
	{
		GLbitfield mapFlags = GL_MAP_READ_BIT
			| GL_MAP_WRITE_BIT
			| GL_MAP_PERSISTENT_BIT
			| GL_MAP_COHERENT_BIT;

		GLbitfield createFlags = mapFlags | GL_DYNAMIC_STORAGE_BIT;


		auto size = sizeof(Struct);


		vertexCount = vertices.size();

		glBindBuffer(target , bufferId);


		if (false)
		{
			glBufferData(target, vertexCount * sizeof(Struct), vertices.data(), usage);
		} else
		{
			glBufferStorage(target, vertexCount * sizeof(Struct), nullptr , createFlags);
			auto pointerToPinnedMemory = static_cast<Struct*>(glMapBufferRange(target,
				0,
				vertexCount * sizeof(Struct),
				mapFlags
			));
			memcpy(pointerToPinnedMemory, vertices.data(), vertexCount * sizeof(Struct));
		}
	}
};


