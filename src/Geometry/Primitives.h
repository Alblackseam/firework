#pragma once

#include "Common.h"
#include "Graphics/Graphics.h"

namespace col
{

struct Circle
{
	static void render()
	{
		glBegin(GL_TRIANGLE_FAN);

		//glColor4ub(155,155,255,255);
		glVertex3f(0, 0, 0);

		static constexpr auto count = 32;
		static constexpr auto twoPi = 2 * glm::pi<float>();

		for (int i = 0; i <= count; ++i)
			glVertex3f(cos(twoPi * i / count), sin(twoPi * i / count), 0);

		glEnd();
	}

	bool isInside(float x, float y) const
	{
		return x * x + y * y <= 1;
	}
};


struct Square
{
	static void render()
	{
		glBegin(GL_TRIANGLE_STRIP);

		glTexCoord2f(0, 0);
		glVertex3f(-1, -1, 0);

		glTexCoord2f(0, 1);
		glVertex3f(-1, 1, 0);

		glTexCoord2f(1, 0);
		glVertex3f(1, -1, 0);

		glTexCoord2f(1, 1);
		glVertex3f(1, 1, 0);

		glEnd();
	}

	bool isInside(float x, float y) const
	{
		if (x >= -1)
			if (x <= 1)
				if (y >= -1)
					if (y <= 1)
						return true;
		return false;
	}
};

}

