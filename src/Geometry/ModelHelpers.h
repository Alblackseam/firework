#pragma once

#include <string>
#include <memory>

#include "Common.h"
#include "Place.h"
#include "Model.h"
// #include "Shader.h"
// #include "Mesh.h"


struct ModelAndPlace
{
	std::shared_ptr<Learn::Model> model;
	glm::mat4 place;
};
