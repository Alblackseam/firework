#pragma once

#define GL_GLEXT_PROTOTYPES
#include <GL/glu.h>


template<typename Vertex>
struct AVertexBuffer
{
	using VertexType = Vertex;

	GLuint bufferId{};
	//GLuint vao{};
	GLenum usage;
	GLuint vertexCount{};
	int components;

	AVertexBuffer(int components, GLenum usage) // GL_STATIC_DRAW
		: components{components}
		, usage{usage}
	{
		glGenBuffers(1, &bufferId);
		//glGenVertexArrays(1, &vao);
	}

	~AVertexBuffer()
	{
		if (bufferId)
			glDeleteBuffers(1, &bufferId);

//		if (vao)
//			glDeleteVertexArrays(1, &vao);
	}


//	void setData(const GLfloat* vertexBufferData, int sizeBytes)
//	{
//		glBindBuffer(GL_ARRAY_BUFFER, vbo);
//		glBufferData(GL_ARRAY_BUFFER, sizeBytes, vertexBufferData, usage);
//	}

	void setNullData(int newVertexCount)
	{
		//vertexCount = newVertexCount;
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * newVertexCount, nullptr, usage);
	}

	void setData(const std::vector<Vertex>& vertices)
	{
		vertexCount = vertices.size();
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vertex), vertices.data(), usage);
	}

	void appendData(const std::vector<Vertex>& vertices)
	{
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);

		GLintptr offset = vertexCount * sizeof(Vertex);
		glBufferSubData(GL_ARRAY_BUFFER, offset, vertices.size() * sizeof(Vertex), vertices.data());

		vertexCount += vertices.size();
	}


	void bind()
	{
		//glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	}

	void unbind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//glBindVertexArray(0);
	}
};



