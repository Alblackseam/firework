#pragma once

#include "Math/Vector.h"
#include <string>
#include <set>
#include <deque>
#include <memory>

#include "Common.h"


struct VertexPosUV
{
	Math::Vector4f position;
	Math::Vector2f uv;


	static void enableAttribArrays()
	{
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
	}

	static void setAttribPointers()
	{
		auto posOffset = offsetof(VertexPosUV, position);
		auto uvOffset = offsetof(VertexPosUV, uv);

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(VertexPosUV), (GLvoid*)posOffset);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexPosUV), (GLvoid*)uvOffset);
	}

	static void disableAttribArrays()
	{
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
	}
};



struct VertexPosColor4UV
{
	Math::Vector3f position;
	Math::Vector4f color;
	Math::Vector2f uv;

	static const int components = 4;

	static void enableAttribArrays()
	{
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
	}

	static void setAttribPointers()
	{
		auto posOffset = offsetof(VertexPosColor4UV, position);
		auto colorOffset = offsetof(VertexPosColor4UV, color);
		auto uvOffset = offsetof(VertexPosColor4UV, uv);

		glVertexAttribPointer(0, components, GL_FLOAT, GL_FALSE, sizeof(VertexPosColor4UV), (GLvoid*)posOffset);
		glVertexAttribPointer(1, components, GL_FLOAT, GL_FALSE, sizeof(VertexPosColor4UV), (GLvoid*)colorOffset);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexPosColor4UV), (GLvoid*)uvOffset);
	}

	static void disableAttribArrays()
	{
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}


};


