#include "Game/GameState.h"
#include <nlohmann/json.hpp>
#include <fstream>

namespace game {

bool GameState::saveToJson()
{
	//using namespace jsoncons;

	std::string fileName{"player.json"};

	std::ofstream os(fileName, std::ios_base::trunc);
	assert(os);

	nlohmann::json js;
	//compact_json_stream_encoder encoder(os); // no indent

	auto game = js["game"];
	game["name"] = name;


	auto states = game["states"];

	for (auto& state: playerStates) {
		//state.saveToJson(states.push_back("state"));
	}


	/*
	encoder.key("game");
	encoder.begin_object();

	encoder.key("name");
	encoder.string_value(name.c_str());


	encoder.begin_array();
	for (auto& state: playerStates) {
		state.saveToJson(encoder);
	}
	encoder.end_array();

	encoder.end_object();
	*/
	return true;
}


bool GameState::loadFromJson()
{
	return false;
}

} // namespace game
