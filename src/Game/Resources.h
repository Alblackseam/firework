#pragma once
#include <string>
#include <array>

namespace game {

struct Resources
{
	static constexpr int ResourcesTypes = 5;

	std::array<int, ResourcesTypes> count;
};

} // namespace Game
