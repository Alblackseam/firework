#include <cassert>
#include "Game.h"
#include "Graphics/Button.h"
#include "Graphics/ButtonDesc.h"

Game::Game(Scene& scene_)
	: scene { scene_ }
	, tm { scene.textureManager }
{
	gameState.name = "AL_1";
	gameState.playerStates.emplace_back();
	gameState.playerStates[0].playerName = "BlackSeam";
	gameState.playerStates[0].resources.count[0] = 100;
	gameState.playerStates[0].resources.count[1] = 101;
	gameState.playerStates[0].resources.count[2] = 102;
	gameState.playerStates[0].resources.count[3] = 103;
	gameState.playerStates[0].resources.count[4] = 104;

	gameState.playerStates[0].places.emplace_back();
	gameState.playerStates[0].places[0].type = game::Place::Type::Town;
	gameState.playerStates[0].places[0].cells[0] = 45;
	gameState.playerStates[0].places[0].cells[1] = 46;
	gameState.playerStates[0].places[0].cells[2] = 38;
}

void Game::init()
{
	using namespace col;

	scene.onHexCenterClicked.emplace_back([this](const auto& args){onHexCenterClicked(args); });
	scene.onHexEdgeClicked.emplace_back([this](const auto& args){onHexEdgeClicked(args); });
	scene.onHexCrossClicked.emplace_back([this](const auto& args){onHexCrossClicked(args); });

	scene.onHexCenterMoved.emplace_back([this](const auto& args){onHexCenterMoved(args); });
	scene.onHexEdgeMoved.emplace_back([this](const auto& args){onHexEdgeMoved(args); });
	scene.onHexCrossMoved.emplace_back([this](const auto& args){onHexCrossMoved(args); });

	vec4 colorFromTexture = {1, 1, 1, 0};

	scene.camera.place.p.z = 8;

	int i = 0;
	for (i = 0; i < scene.textures.size(); ++i)
	{
		scene.appendButton({
			{ {{90.0f, i * 10.0f}}, {{9, 9}} },
			[this, i](){
				std::cout << "button " << i << " clicked" << std::endl;
				currentTextureIndex = i;
			},
			colorFromTexture,
			scene.textures[i]->getNativeHandle()
		});
	}

	scene.appendButton({
		{ {{90.0f, i * 10.0f}}, {{9, 9}} },
		[this](){
			scene.field.saveMap();
			std::cout << "saveMap" << std::endl;
		},
		vec4{1, 0.1f, 0.1f, 1.0f},
		0});

	++i;
	scene.appendButton({
		{ {{90.0f, i * 10.0f}}, {{9, 9}} },
		[this](){
			scene.field.loadMap();
			std::cout << "loadMap" << std::endl;
		},
		vec4{0, 1.0f, 0.1f, 1.0f},
						   0});

	++i;
	scene.appendLabel( "1234", { {{90.0f, i * 10.0f}}, {{9, 9}} }
		, "1234"
		, vec4{{0, 1.0f, 0.1f, 0.9f}});

	scene.appendLabel( "Test_1", { {{0.0f, 0.0f}}, {{19, 19}} }
		, "Test 1"
		, vec4{{1.0f, 0.0f, 0.0f, 0.9f}});


	glowdotTextureId = tm.get("Glowdot.png")->getNativeHandle();

	gameState.saveToJson();
}

static glm::mat4 translateScaleMatrix(const glm::vec3& trans, float scale)
{
	//		auto place = Place::identity();
	//		place.p = *currentCross;
	//		place.s = vec3::one() * 0.3f;
	//		auto model = place.worldMatrixTransposed<glm::mat4x4>();
	//		return model;
	return 	glm::scale(glm::translate(glm::identity<glm::mat4>(), trans), glm::vec3{1, 1, 1} * scale);
}

void Game::render(col::Graphics& graphics)
{
	assert (scene.texturedShader);
	auto shader = scene.texturedShader;

	glEnable(GL_DEPTH_TEST);

	glUseProgram(shader);
	glUniformMatrix4fv(glGetUniformLocation(shader, "projMatrix"), 1, GL_FALSE, glm::value_ptr(graphics.proj));

	auto loc = glGetUniformLocation(scene.texturedShader, "color");
	glUniform4f(loc, 1, 0, 0, 0.2f);

	glBindTexture(GL_TEXTURE_2D, glowdotTextureId);


	std::vector<glm::mat4x4> places;

	if (currentCross) {
		places.emplace_back(translateScaleMatrix(toGlm(*currentCross), 0.4f));
	} else {
		if (currentHexCenter)
			places.emplace_back(translateScaleMatrix(toGlm(*currentHexCenter), 0.7f));

		if (currentEdge)
			places.emplace_back(translateScaleMatrix(toGlm(*currentEdge), 0.2f));
	}

	for (const auto& modelMatrix: places)
	{
		glUniformMatrix4fv(glGetUniformLocation(shader, "viewMatrix"),  1, GL_FALSE, glm::value_ptr(graphics.view * modelMatrix));
		col::Circle::render();
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glUseProgram(0);
}

void Game::onHexCenterClicked(const std::vector<HexDistanceInfo> & hitInfo)
{
	assert(hitInfo.size() >= 1);
	auto ind = hitInfo[0].indices;
	//scene.field.hexagons[ind.first][ind.second]->type = currentTextureIndex;
}

void Game::onHexEdgeClicked(const std::vector<HexDistanceInfo> &)
{
}

void Game::onHexCrossClicked(const std::vector<HexDistanceInfo> &)
{
}

void Game::onHexCenterMoved(const std::vector<HexDistanceInfo> & di)
{
	assert(di.size() >= 1);
	currentEdge.reset();
	currentCross.reset();
	currentHexCenter = { di[0].center.x, di[0].center.y, 0.01f };

	auto it = scene.labels.find("Test_1");
	if (it != std::end(scene.labels))
	{
		auto [y, x] = di[0].indices;
		auto id = scene.field.hexInfo[y][x].hexId;
		it->second.setText("[" + std::to_string(x) + "," + std::to_string(y) + "] " + std::to_string(id));
	}
}

void Game::onHexEdgeMoved(const std::vector<HexDistanceInfo> & di)
{
	currentHexCenter.reset();
	currentCross.reset();

	if (di.size() >= 2)
	{
		auto edgeCenter = (di[0].center + di[1].center) * 0.5f;
		currentEdge = { edgeCenter.x, edgeCenter.y, 0.01f };
	}
}

void Game::onHexCrossMoved(const std::vector<HexDistanceInfo> & di)
{
	currentHexCenter.reset();
	currentHexCenter.reset();
	if (di.size() >= 3)
	{
		auto crossCenter = (di[0].center + di[1].center + di[2].center) / 3;
		currentCross = { crossCenter.x, crossCenter.y, 0.01f };
	}
}
