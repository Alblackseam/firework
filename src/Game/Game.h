#pragma once
#include "Vector.h"
#include <optional>
#include "Game/GameState.h"
#include "Scene.h"


struct Game // SceneController
{
	explicit Game(Scene& scene);
	Scene& scene;
	TextureManager& tm;

	void init();
	void render(col::Graphics &graphics);


	std::string currentPlayerName;
	void buildVillage(int cellId);
	void buildTown();
	void buildRoad();

protected:
	void onHexCenterClicked(const std::vector<HexDistanceInfo>&); // Field::Handler
	void onHexEdgeClicked(const std::vector<HexDistanceInfo>&);
	void onHexCrossClicked(const std::vector<HexDistanceInfo>&);

	void onHexCenterMoved(const std::vector<HexDistanceInfo>&);
	void onHexEdgeMoved(const std::vector<HexDistanceInfo>&);
	void onHexCrossMoved(const std::vector<HexDistanceInfo>&);

	game::GameState gameState;
	int currentTextureIndex = 0;

	int glowdotTextureId = 0;
	std::optional<Math::Vector3f> currentHexCenter;
	std::optional<Math::Vector3f> currentEdge;
	std::optional<Math::Vector3f> currentCross;
};


