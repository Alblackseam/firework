#pragma once
#include "Vector.h"
#include <string>
#include <vector>
#include "Game/Place.h"
#include "Game/Road.h"
#include "Game/Resources.h"

#include <nlohmann/json.hpp>


namespace game {

struct PlayerState
{
	std::string playerName;

	std::vector<Place> places;
	std::vector<Road> roads;
	Resources resources;

	//bool saveToJson(jsoncons::compact_json_stream_encoder& encoder);
	bool loadFromJson();
};

} // namespace Game
