#pragma once

#include "Common.h"
#include "Math/Place.h"
#include "Graphics/Graphics.h"

#include <map>
#include <list>
#include <vector>
#include <memory>
#include <functional>

using vec2 = Math::Vector2f;
using vec3 = Math::Vector3f;
using vec4 = Math::Vector4f;




struct HexDistanceInfo
{
	float distance;
	Math::Vector2f center;
	std::pair<int, int> indices;
};


enum class HexType : unsigned char {
	Water, Clay, Stone, Sheep, Wheat, Forest, Mountain
};


struct HexInfo
{
	int hexId {};
	HexType hexType {HexType::Water};
	vec2 position {};
};


struct Field
{
	using Handler = std::function<void(const std::vector<HexDistanceInfo>&)>;

	Place place{ {1,0,0}, {0,1,0}, {0,0,1}, {0,0,0} };

	std::map<int, std::map<int, HexInfo>> hexInfo;
	static constexpr int halfRowCount = 4;
	int hexagonsCount = 37;
	std::vector<HexDistanceInfo> nearHexes;


	void init();
	void saveMap(std::string fileName = "field.json");
	void loadMap(std::string fileName = "field.json");

	void onMouseMove(glm::vec2 fieldCoords);
	void leftMouseReleased(glm::vec2 fieldCoords);

	void hitCheck(glm::vec2 fieldCoords, Handler onCenter, Handler onEdge, Handler onCross);

	void renderHelpers(col::Graphics& graphics);

	//std::vector<std::shared_ptr<sf::Texture>> textures;


	static constexpr int hexTypeCount = 7;

	static constexpr std::array<std::string_view, hexTypeCount> hexTypeName {
		"Water", "Clay", "Stone", "Sheep", "Wheat", "Forest", "Mountain"
	};

	static constexpr std::string_view getHexName(HexType type)
	{
		return hexTypeName[static_cast<int>(type)];
	}
};


