#pragma once


#include <map>
#include <list>
#include <vector>
#include <string>
#include <memory>

#include "Common.h"
#include "Field.h"
#include "ModelHelpers.h"


struct FieldController
{
	void makeModelsFromField(std::map<std::string, std::shared_ptr<Learn::Model>> models, const Field& field );
	void render(Shader lightingShader);

	std::map<int, std::shared_ptr<ModelAndPlace>> modelAndPlaces;
};



