#pragma once

#include <string>
#include <set>
#include <list>
#include <deque>
#include <memory>
#include <random>
#include <chrono>
#include <functional>
#include <exception>

#include "Common.h"

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include "Math/Place.h"
#include "Geometry/ABuffer.h"
#include "Geometry/AVertexBuffer.h"
#include "Graphics/Button.h"
#include "Graphics/ButtonDesc.h"
#include "SfmlButton.h"
#include "Graphics/Framebuffer.h"
#include "Graphics/Texture.h"
#include "Graphics/Camera.h"
#include "Graphics/Graphics.h"
#include "Game/Field.h"
#include "Graphics/FreeTypeFace.h"
#include "Graphics/Label.h"
#include <filesystem>

//#include "Shader.h"
//#include "Mesh.h"
#include "Geometry/Model.h"
#include "Geometry/ModelHelpers.h"
#include "Game/FieldController.h"

struct TextureManager
{
	using TexureShared = std::shared_ptr<sf::Texture>;

	TexureShared get(const std::string& fileName) {
		auto it = texturesMap.find(fileName);
		if (it != texturesMap.end())
			return it->second;
		auto res = texturesMap.emplace(fileName, load(fileName));
		return res.first->second;
	}

	TexureShared load(const std::string& fileName)
	{
		auto texture = std::make_shared<sf::Texture>();
		texture->loadFromFile(std::filesystem::path{"Resources"} / fileName);
		return texture;
	}

	std::unordered_map<std::string, std::shared_ptr<sf::Texture>> texturesMap;
};


struct Scene
{
	using Handlers = std::list<Field::Handler>;


	Scene(sf::RenderWindow& window);

	void init();

	void onResize(int w, int h);

	bool onKeyPressed(const sf::Event::KeyEvent& keyEvent) {
		pressedKeys.insert(keyEvent.code);
		return false;
	}

	bool onKeyReleased(const sf::Event::KeyEvent& keyEvent) {
		pressedKeys.erase(keyEvent.code);
		return false;
	}

	void onMouseMoved(const sf::Event::MouseMoveEvent& moveEvent);

	void onMouseButtonPressed(const sf::Event::MouseButtonEvent& mouseButton) {}

	void onMouseButtonReleased(const sf::Event::MouseButtonEvent& mouseButton);

	bool isKeyDown(sf::Keyboard::Key keyCode) const {
		return pressedKeys.find(keyCode) != pressedKeys.end();
	}



	void clear();

	void render(std::function<void(col::Graphics &)> drawer);

	void update(float deltaTime);


	Handlers onHexCenterClicked;
	Handlers onHexEdgeClicked;
	Handlers onHexCrossClicked;

	Handlers onHexCenterMoved;
	Handlers onHexEdgeMoved;
	Handlers onHexCrossMoved;

	void appendButton(col::ButtonDesc&&);
	void appendLabel(std::string name, const col::Rect2f &rect, std::string text, const vec4& color);
	void updateButtonsPlace();

	sf::RenderWindow& window;

	std::vector<std::shared_ptr<col::Button<col::Square>>> buttons2;
	std::vector<std::shared_ptr<col::Button<col::Circle>>> buttons3;
	std::vector<std::shared_ptr<col::Button<col::Circle, false>>> buttons4;
	std::vector<std::shared_ptr<col::Button<col::Square, false>>> buttons5;

	Camera camera;

	std::vector<std::shared_ptr<sf::Texture>> textures;
	//std::shared_ptr<sf::Texture> shipFighterDiffTexture;

	TextureManager textureManager;

	Field field;
	FieldController fieldController;

	GLuint colorShader {};
	GLuint texturedShader {};
	GLuint textured_2_Shader {};
	GLuint selectObjectShader {};

	GLuint shipFighterShader {};
	GLuint _lightingShader {};

	std::map<std::string, std::shared_ptr<Learn::Model>> models;
protected:
	std::map<std::string, std::shared_ptr<ModelAndPlace>> modelAndPlaces;


	int framesRendered = 0;
	float oneSecondTimeCounter = 0;
	float fps = 0.0f;

	sf::Texture glowTexture;
	col::Graphics graphics;


	sf::Shader vertShader;

	std::set<sf::Keyboard::Key> pressedKeys;

	sf::Font font;

	std::shared_ptr<FreeTypeLibrary> freeTypeLibrary;
	std::shared_ptr<FreeTypeFaceRenderer> freeTypeFaceRenderer;
	//std::shared_ptr<TextRenderer> textRenderer;

public: // TODO: removeme
	std::map<std::string, col::Label<false>> labels;
};

