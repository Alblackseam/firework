#pragma once
#include <string>
#include <array>

namespace game {

struct Place
{
	enum class Type { Village, Town };
	static constexpr std::array<std::string_view,2> TypeNames = { "Village", "Town" };

	Type type;
	std::array<int, 3> cells;

	std::string_view getType() const
	{
		return TypeNames[static_cast<int>(type)];
	}
};

} // namespace Game
