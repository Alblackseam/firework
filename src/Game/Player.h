#pragma once
#include "Vector.h"
#include <string>
#include <vector>
#include "Game/Place.h"
#include "Game/Road.h"
#include "Game/Resources.h"

namespace game {

struct Player
{
	Player() = default;

	std::string name;

	std::vector<Place> places;
	std::vector<Road> roads;
	Resources resources;

	bool saveToJson();
	bool loadFromJson();
};

} // namespace Game
