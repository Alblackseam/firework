#include "FieldController.h"

void FieldController::makeModelsFromField(std::map<std::string, std::shared_ptr<Learn::Model> > models, const Field &field)
{
	for (auto& [j, row] : field.hexInfo)
	{
		for (auto& [i, cell] : row)
		{
			std::string hexName = Field::getHexName(cell.hexType).data();
			//std::cout << "hexName: " << hexName << std::endl;
			auto it = models.find(hexName);
			if (it == models.end())
				continue;

			auto place = glm::translate(glm::mat4x4{1}, glm::vec3{cell.position.x, cell.position.y, -0.1f});
			place = glm::scale(place, glm::vec3{1} * 0.2f);
			place = glm::rotate(place, 3.1415926f / 2, glm::vec3{1,0,0});

			modelAndPlaces.insert_or_assign(cell.hexId, std::make_shared<ModelAndPlace>(ModelAndPlace{it->second, place}));
		}
	}

}

void FieldController::render(Shader lightingShader)
{
	for (auto& [name, mod] : modelAndPlaces)
	{
		lightingShader.setMat4("model", mod->place);
		mod->model->Draw(lightingShader);
	}
}

