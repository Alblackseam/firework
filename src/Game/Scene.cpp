#include "Scene.h"
#include <fstream>
#include <thread>
#include <iostream>
#include <chrono>
#include <memory>
#include <algorithm>
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/scene.h"

#include "Graphics/CameraHelpers.h"
#include "utility/StringUtils.h"
#include "Graphics/OpenGlHelpers.h"
#include "Graphics/Graphics.h"
#include "Random.h"


Scene::Scene(sf::RenderWindow& window_)
	: window{ window_ }
	, camera {60 * M_PI / 180, 0.1f, 1000.0f, Place{}, static_cast<float>(window.getSize().x), static_cast<float>(window.getSize().y) }
{
	camera.place.p = {{0, 0, 12}};
	camera.place.f = {0, 0.01f, -1.0f};
	camera.place.f.normalize();
	camera.place.u = {0, 1, 0};
	camera.place.calcRight();
}

inline vec4 toVec4(const vec3& a)
{
	return vec4{a.x, a.y, a.z, 1};
}

inline vec3 toVec3(const vec3& a)
{
	return a;
}


void loadModel(std::string path)
{
	using namespace Assimp;
	using namespace std;

	Assimp::Importer import;
	const aiScene *scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		cout << "ERROR::ASSIMP::" << import.GetErrorString() << endl;
		return;
	}
	auto directory = path.substr(0, path.find_last_of('/'));

	// processNode(scene->mRootNode, scene);
}

void Scene::init()
{
	using namespace sf;
	window.setVerticalSyncEnabled(true);
	glShadeModel(GL_FLAT);

	colorShader = loadShaders("Shaders/colorShader.vert", "Shaders/colorShader.frag");
	texturedShader = loadShaders("Shaders/textured.vert", "Shaders/textured.frag");
	textured_2_Shader = loadShaders("Shaders/textured_2.vert", "Shaders/textured_2.frag");
	selectObjectShader = loadShaders("Shaders/id.vert", "Shaders/id.frag");

	shipFighterShader = loadShaders("Shaders/textured_3.vert", "Shaders/textured_3.frag");
	_lightingShader = loadShaders("Shaders/TexturedLightingShader.vert", "Shaders/TexturedLightingShader.frag");


	if (!glowTexture.loadFromFile("Resources/Glowdot.png"))
		throw std::runtime_error("can't load texture");

	for (const auto& fileName : std::array{"water.jpg", "clay.png", "stone.png", "sheep.png", "wheat.png", "forest.png", "mountain.png"})
		textures.emplace_back(textureManager.get(fileName));

//	shipFighterDiffTexture = textureManager.get("ShipFighter.jpg");

	field.init();
	field.loadMap();

	//https://habr.com/ru/post/342982/
	if (!font.loadFromFile("SourceCodePro-Regular.ttf"))
		std::cout << "error..." << std::endl;

	freeTypeLibrary = std::shared_ptr<FreeTypeLibrary>(new FreeTypeLibrary());
	freeTypeFaceRenderer = std::shared_ptr<FreeTypeFaceRenderer>(new FreeTypeFaceRenderer(freeTypeLibrary));
	freeTypeFaceRenderer->setPixelSize(36);
	//freeTypeFaceRenderer->getCharacter('a');


	auto resNames = std::array{
			"Water"s,
			"Clay"s,
			"Sheep"s,
			"Stone"s,
			"Wheat"s,
			"Village"s,
			"Town"s,
			"Forest"s
		};

	for (auto& resName : resNames)
		models[resName] = std::make_shared<Learn::Model>("Resources/" + resName + ".obj");

	fieldController.makeModelsFromField(models, field);
}

void Scene::onResize(int w, int h)
{
	window.pushGLStates();

	camera.width = w;
	camera.height = h;

	updateButtonsPlace();

	window.popGLStates();
}

void Scene::onMouseMoved(const sf::Event::MouseMoveEvent&)
{
	auto pos = sf::Mouse::getPosition(window); //cout << pos.x << ", " << pos.y << " height:" << height << endl;
	pos.y = camera.height - pos.y;
	auto fieldCoords = graphics.PlaneUnproject(pos);

	field.hitCheck(fieldCoords, [this](const auto& args) mutable {
			for (auto& handler: onHexCenterMoved)
				handler(args);
		}, [this](const auto& args) mutable {
			for (auto& handler: onHexEdgeMoved)
				handler(args);
		}, [this](const auto& args) mutable{
			for (auto& handler: onHexCrossMoved)
				handler(args);
		});
}

void Scene::onMouseButtonReleased(const sf::Event::MouseButtonEvent &mouseButton)
{
	using namespace std;

	if (mouseButton.button == sf::Mouse::Left)
	{
		auto pos = sf::Mouse::getPosition(window);
		pos.y = camera.height - pos.y;
		auto fieldCoords = graphics.PlaneUnproject(pos);


		for (auto& button : buttons2)
			onLeftMouseButtonReleased(fieldCoords, *button);

		for (auto& button : buttons3)
			onLeftMouseButtonReleased(fieldCoords, *button);

		for (auto& button : buttons4)
			onLeftMouseButtonReleased(fieldCoords, *button);

		for (auto& button : buttons5)
			onLeftMouseButtonReleased(glm::vec3( pos.x, pos.y, 0.001f), *button, graphics);

		field.hitCheck(fieldCoords, [this](const auto& args) mutable {
				for (auto& handler: onHexCenterClicked)
					handler(args);
			}, [this](const auto& args) mutable {
				for (auto& handler: onHexEdgeClicked)
					handler(args);
			}, [this](const auto& args) mutable{
				for (auto& handler: onHexCrossClicked)
					handler(args);
			});
	}
}

void Scene::clear()
{
	window.pushGLStates();

	glClearColor(0.0f, 0.1f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	window.popGLStates();
}

void Scene::render(std::function<void(col::Graphics&)> drawer)
{
	using namespace std;
	window.pushGLStates();

	glViewport(0, 0, static_cast<GLsizei>(camera.width), static_cast<GLsizei>(camera.height));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(camera.fovDegrees(), static_cast<GLfloat>(camera.width) / static_cast<GLfloat>(camera.height), camera.zNear, camera.zFar);


	glMatrixMode(GL_MODELVIEW);
	auto viewTarget = camera.place.p + camera.place.f;
	gluLookAt(camera.place.p.x, camera.place.p.y, camera.place.p.z,
		viewTarget.x, viewTarget.y, viewTarget.z,
		camera.place.u.x, camera.place.u.y, camera.place.u.z);


	GLfloat mp[16], mv[16];
	glGetFloatv(GL_PROJECTION_MATRIX, mp);
	glGetFloatv(GL_MODELVIEW_MATRIX, mv);

	graphics.proj = glm::make_mat4(mp);
	graphics.view = glm::make_mat4(mv);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);

	//field.renderHexes(graphics, texturedShader);


	glm::vec3 lightPos {0,10,10};


	Shader lightingShader{ _lightingShader };
	lightingShader.use();
	lightingShader.setVec3("light.position", lightPos);
	lightingShader.setVec3("viewPos", toGlm(camera.place.p));

	// light properties
	lightingShader.setVec3("light.ambient", 0.4f, 0.4f, 0.4f);
	lightingShader.setVec3("light.diffuse", 0.7f, 0.7f, 0.7f);
	lightingShader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);

	// material properties
	lightingShader.setFloat("material.shininess", 64.0f);

	// view/projection transformations
	glm::mat4 projection = graphics.proj;
	glm::mat4 view = graphics.view;
	lightingShader.setMat4("projection", projection);
	lightingShader.setMat4("view", view);


	for (auto& [name, mod] : modelAndPlaces)
	{
		lightingShader.setMat4("model", mod->place);
		mod->model->Draw(lightingShader);
	}

	fieldController.render(lightingShader);



#if false
	Shader lightingShader{ _lightingShader };
	lightingShader.use();

	lightingShader.setVec3("light.position", lightPos);
	lightingShader.setVec3("viewPos", toGlm(camera.place.p));

	static float curTime = 0;
	curTime += 0.01f;

	// light properties
	glm::vec3 lightColor;
	lightColor.x = 1; //static_cast<float>(sin(curTime * 2.0));
	lightColor.y = 1; //static_cast<float>(sin(curTime * 0.7));
	lightColor.z = 1; // static_cast<float>(sin(curTime * 1.3));
	glm::vec3 diffuseColor = lightColor   * glm::vec3(0.5f); // decrease the influence
	glm::vec3 ambientColor = diffuseColor * glm::vec3(0.2f); // low influence
	lightingShader.setVec3("light.ambient", ambientColor);
	lightingShader.setVec3("light.diffuse", diffuseColor);
	lightingShader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);

	// material properties
	lightingShader.setVec3("material.ambient", 1.0f, 0.5f, 0.31f);
	lightingShader.setVec3("material.diffuse", 1.0f, 0.5f, 0.31f);
	lightingShader.setVec3("material.specular", 0.5f, 0.5f, 0.5f); // specular lighting doesn't have full effect on this object's material
	lightingShader.setFloat("material.shininess", 32.0f);

	// view/projection transformations
	glm::mat4 projection = graphics.proj;
	glm::mat4 view = graphics.view;
	lightingShader.setMat4("projection", projection);
	lightingShader.setMat4("view", view);

	// world transformation
	glm::mat4 model = glm::mat4(1.0f);
	lightingShader.setMat4("model", model);

	lModel->Draw(lightingShader);
#endif



#if false
	Shader sh_textured_3{ shipFighterShader };
	sh_textured_3.use();

	glm::mat4 projection = graphics.proj; //  glm::perspective(camera.fov, camera.getAspect(), camera.zNear, camera.zFar);
	glm::mat4 view = graphics.view; // camera.place.worldMatrix<glm::mat4>(); //  GetViewMatrix();
	sh_textured_3.setMat4("projection", projection);
	sh_textured_3.setMat4("view", view);

	// render the loaded model
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));	// it's a bit too big for our scene, so scale it down
	sh_textured_3.setMat4("model", model);

	lModel->Draw(sh_textured_3);
#endif


	glUseProgram(0);



	renderCoordSystem();

	auto pos = sf::Mouse::getPosition(window); //cout << pos.x << ", " << pos.y << " height:" << height << endl;
	pos.y = camera.height - pos.y;
	auto fieldCoords = graphics.PlaneUnproject(pos);
	glm::vec3 fieldCoords3{fieldCoords, -0.1f};
	glm::vec3 underFieldCoords3{fieldCoords, 1};


	//auto mouseDirection = toMath(graphics.getRayFromMouse(glm::vec2{x / width, -y / height}));
	//float distToField = dot(-field.place.f, field.place.p - camera.p);
	//auto M = distToField / -dot(mouseDirection, field.place.f);
	//
	//cout << distToField << " M:" << M << endl;

	glBegin(GL_LINES);
		glColor4f(1,1,1,1);
		glVertex3f(0, 0, 0);

		glColor4f(1,0,0,1);
		glVertex3fv(glm::value_ptr(fieldCoords3));


		glColor4f(1,1,0,1);
		glVertex3fv(glm::value_ptr(fieldCoords3));

		glColor4f(1,1,1,1);
		glVertex3fv(glm::value_ptr(underFieldCoords3));


		//glColor4ub(255,255,255,255);
		//glVertex3f(0,0,1);
		//glVertex3fv(camera.p.data().data());
		//glVertex3fv((camera.p + mouseDirection * M * 0.9f).data().data());
		//glColor4ub(0,255,0,255);
		//glVertex3fv((camera.p + mouseDirection * M).data().data());
	glEnd();

	drawer(graphics);


	for (auto button : buttons2)
		if (button)
			button->render(graphics);
	for (auto button : buttons3)
		if (button)
			button->render(graphics);

	glDisable(GL_DEPTH_TEST);
	for (auto button : buttons4)
		if (button)
			button->render(graphics);


	glUseProgram(texturedShader);
	glUniformMatrix4fv(glGetUniformLocation(texturedShader, "projMatrix"), 1, GL_FALSE, glm::value_ptr(graphics.proj));
	for (auto button : buttons5)
		if (button)
		{
			auto view = button->place.worldMatrixTransposed<glm::mat4x4>();
			glUniformMatrix4fv(glGetUniformLocation(texturedShader, "viewMatrix"),  1, GL_FALSE, glm::value_ptr(view));
			button->render(graphics);
		}
	glBindTexture(GL_TEXTURE_2D, 0);



	glUseProgram(textured_2_Shader);
	auto projMatriOffset = glGetUniformLocation(textured_2_Shader, "projMatrix");
	glUniformMatrix4fv(projMatriOffset, 1, GL_FALSE, glm::value_ptr(graphics.proj));

	glDisable(GL_DEPTH_TEST);

	for (auto& [name, label] : labels)
		label.render(graphics, textured_2_Shader);


	glUseProgram(0);
	glEnable(GL_DEPTH_TEST);


	window.popGLStates();

	++framesRendered;
}

void Scene::update(float dt)
{
	oneSecondTimeCounter += dt;
	if (oneSecondTimeCounter >= 1.0f)
	{
		//fps += 1;
		fps = framesRendered / oneSecondTimeCounter;
		framesRendered = 0;
		oneSecondTimeCounter = 0.0f;
	}

	if (isKeyDown(sf::Keyboard::Escape))
		window.close();

	if (isKeyDown(sf::Keyboard::D))
		camera.place.strafeRight(0.1f);

	if (isKeyDown(sf::Keyboard::A))
		camera.place.strafeLeft(0.1f);

	if (isKeyDown(sf::Keyboard::W))
		//camera.place.strafeUp(0.1f);
		camera.place.onward(0.1f);

	if (isKeyDown(sf::Keyboard::S))
		//camera.place.strafeDown(0.1f);
		camera.place.onward(-0.1f);

	if (isKeyDown(sf::Keyboard::Right))
		//camera.place.turnRight(0.03f);
		camera.rotateAroundVector({0,0,1}, -0.04f);

	if (isKeyDown(sf::Keyboard::Left))
		//camera.place.turnLeft(0.03f);
		camera.rotateAroundVector({0,0,1}, 0.04f);

	if (isKeyDown(sf::Keyboard::Up))
		//camera.place.onward(0.1f); //camera.turnUp(0.03f);
		camera.rotateAroundVector(camera.place.r, 0.03f);

	if (isKeyDown(sf::Keyboard::Down))
		//camera.place.backward(0.1f); //camera.turnDown(0.03f);
		camera.rotateAroundVector(camera.place.r, -0.03f);
}



void Scene::updateButtonsPlace()
{
	auto scrHalfSize = camera.viewHalfSizeOnNearPlane();

	for (auto& button : buttons5)
		button->place = calculatePlaceNearPlane(scrHalfSize, camera.zNear, button->desc.rect);

}

void Scene::appendButton(col::ButtonDesc && desc)
{
	using namespace col;

	auto p = calculatePlaceNearPlane(camera.viewHalfSizeOnNearPlane(), camera.zNear, desc.rect);
	buttons5.emplace_back(std::shared_ptr<Button<Square, false>>( new Button<Square, false>{{}, p, desc.handler, desc.color, desc} ));
}

void Scene::appendLabel(std::string name, const col::Rect2f& rect, std::string text, const vec4& color)
{
	//label = std::make_shared<std::remove_reference_t<decltype(*label)>>();

	auto p = col::calculatePlaceNearPlane(camera.viewHalfSizeOnNearPlane(), camera.zNear, rect);

	col::Label<false> label;
	label.freeTypeFaceRenderer = freeTypeFaceRenderer;
	label.setText(text);
	label.place = p;
	label.color = color;

	labels.emplace(std::move(name), std::move(label));


}




