#pragma once
#include <string>
#include <vector>
#include "Game/PlayerState.h"

namespace game {

struct GameState
{
	std::string name;
	std::vector<PlayerState> playerStates;

	bool saveToJson();
	bool loadFromJson();
};

} // namespace game
