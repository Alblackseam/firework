#pragma once
#include <array>
#include <string>

namespace game {

struct Road
{
	Road() = default;

	std::array<int, 2> cells;
};

} // namespace Game
