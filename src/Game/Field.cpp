#include "Field.h"
#include "Math/Random.h"
#include "Graphics/Button.h"
#include <nlohmann/json.hpp>
#include <fstream>
#include <iostream>


void Field::init()
{
	glm::mat4x4 view {1};
	//std::uniform_int_distribution<int> textureRandom( 0, textures.size() - 1 );
	float size = 1;

	auto hexId = 0;
	for (int j = -halfRowCount; j <= halfRowCount; ++j)
	{
		auto rowPos = size * 0.5f * vec2{-halfRowCount * sqrtf(3.0f) * 2 + abs(j) * sqrtf(3.0f), j * 3.0f};

		auto count = 2 * halfRowCount + 1 - abs(j);
		for (int i = 0; i < count; ++i)
		{
			auto cellPos = rowPos + vec2{i * size * 2.0f * sqrtf(3.0f) / 2, 0.0f};

			hexInfo[j][i].hexId = hexId;
			hexInfo[j][i].position = cellPos;
			auto hexTypeIndex = static_cast<int>( rand() * 1.0 * hexTypeCount / RAND_MAX );
			hexInfo[j][i].hexType = static_cast<HexType>(hexTypeIndex);
			++hexId;
		}
	}
}

void Field::onMouseMove(glm::vec2 fieldCoords)
{
	using namespace std;

	//hitCheck(fieldCoords, );

}

void Field::leftMouseReleased(glm::vec2 fieldCoords)
{
	//hitCheck(fieldCoords);
}

void Field::hitCheck(glm::vec2 fieldCoords, Handler onCenter, Handler onEdge, Handler onCross)
{
	std::vector<HexDistanceInfo> distanceInfo;
	distanceInfo.reserve(hexagonsCount);

	for (auto& [j, row] : hexInfo)
	{
		for (auto& [i, cell] : row)
		{
			auto dist = Math::magnitudeSquared(cell.position - vec2{{fieldCoords.x, fieldCoords.y}});

			distanceInfo.emplace_back(HexDistanceInfo{
				dist, cell.position, std::pair{j, i}
				});
		}
	}

	std::sort(distanceInfo.begin(), distanceInfo.end(), [](const auto& a, const auto b) -> bool {
		return a.distance < b.distance;
		});

	nearHexes.resize(3);
	for (int i = 0; i < 3; ++i)
		nearHexes[i] = distanceInfo[i];


	using namespace std;
	auto summ1 = distanceInfo[0].distance;
	auto summ2 = summ1 + distanceInfo[1].distance;
	auto summ3 = summ2 + distanceInfo[2].distance;

	if (summ1 < 0.25f)
	{
		//cout << "center of hex " << summ1 << endl;
		onCenter(distanceInfo);
	} else
	{
		if (summ3 < 3.1f)
		{
			//cout << "middle of triangles" << endl;
			onCross(distanceInfo);
		} else
		{
			if (summ2 < 1.7f)
			{
				//cout << "edge" << endl;
				onEdge(distanceInfo);
			} else
			{
				//cout << "none " << summ1<< endl;
			}
		}
	}
}

void Field::renderHelpers(col::Graphics &graphics)
{
	for (auto& nh : nearHexes)
	{
		glMatrixMode(GL_MODELVIEW);
		vec3 center{{nh.center.x, nh.center.y, 0.01f}};

		auto transAndScale = glm::scale(glm::translate(glm::mat4x4{1}, toGlm(center)), glm::vec3{1} * 0.1f);
		glLoadMatrixf(glm::value_ptr(graphics.view * transAndScale));

		vec4 colorB = {{0.8f, 0.8f, 1, 1}};
		glColor4fv(colorB.data().data());
		col::Circle::render();
	}
}

void Field::saveMap(std::string fileName)
{
	/*
	using namespace jsoncons;

	std::ofstream os(fileName, std::ios_base::trunc);
	assert(os);

	compact_json_stream_encoder encoder(os); // no indent
	encoder.begin_array();

	for (auto& row : hexInfo)
	{
		for (auto& hex : row.second)
		{
			//encoder.key("price");
			auto val = static_cast<int>(hex.second.hexType);
			//encoder.begin_object();
			encoder.int64_value(val);
			//encoder.end_object();

		}
	}

	encoder.end_array();
	*/
}

void Field::loadMap(std::string fileName)
{
	/*
	using namespace jsoncons;

	std::ifstream is(fileName);
	assert(is);
	//json j = json::parse(is);

	json_stream_cursor cursor(is);
	auto view = staj_array<json>(cursor);
	auto it = view.begin();

	for (int j = -halfRowCount; j <= halfRowCount; ++j)
	{
		auto count = 2 * halfRowCount + 1 - abs(j);
		for (int i = 0; i < count; ++i)
		{
			hexInfo[j][i].hexType = static_cast<HexType>(it->as<int>());
			++it;
		}
	}
	*/
}


