#include "Editor.h"
#include "Graphics/Button.h"
#include "Graphics/ButtonDesc.h"
#include <nlohmann/json.hpp>
#include <filesystem>


Editor::Editor(Scene& scene_, std::string jsonPath)
	: scene{scene_}
{
	using namespace nlohmann;
	std::ifstream is(std::filesystem::path{"Resources"} / jsonPath);
	assert(is);

	json j = json::parse(is);
}

void Editor::init()
{
	using namespace col;

	scene.onHexCenterClicked.emplace_back([this](const auto& args){onHexCenterClicked(args); });
	scene.onHexEdgeClicked.emplace_back([this](const auto& args){onHexEdgeClicked(args); });
	scene.onHexCrossClicked.emplace_back([this](const auto& args){onHexCrossClicked(args); });


//	Row { 90, 0, 10, 50 };
//	Button1 {}
//	Button2 {}
//	Row.append(button1);
//	Row.append(button2);

	vec4 colorFromTexture = {1, 1, 1, 0};

	int i = 0;
	for (i = 0; i < scene.textures.size(); ++i)
	{
		scene.appendButton({
			{ {{90.0f, i * 10.0f}}, {{9, 9}} },
			[this, i](){
				std::cout << "button " << i << " clicked" << std::endl;
				currentTextureIndex = i;
			},
			colorFromTexture,
			scene.textures[i]->getNativeHandle()
		});
	}

	scene.appendButton({
		{ {{90.0f, i * 10.0f}}, {{9, 9}} },
		[this](){
			scene.field.saveMap();
			std::cout << "saveMap" << std::endl;
		},
		vec4{1, 0.1f, 0.1f, 1.0f},
		0});

	++i;
	scene.appendButton({
		{ {{90.0f, i * 10.0f}}, {{9, 9}} },
		[this](){
			scene.field.loadMap();
			std::cout << "loadMap" << std::endl;
		},
		vec4{0, 1.0f, 0.1f, 1.0f},
		0});
}

void Editor::onHexCenterClicked(const std::vector<HexDistanceInfo> & hitInfo)
{
	assert(hitInfo.size() >= 1);
	auto ind = hitInfo[0].indices;
	scene.field.hexInfo[ind.first][ind.second].hexType = static_cast<HexType>(currentTextureIndex);

	scene.fieldController.makeModelsFromField(scene.models, scene.field);
}

void Editor::onHexEdgeClicked(const std::vector<HexDistanceInfo> &)
{

}

void Editor::onHexCrossClicked(const std::vector<HexDistanceInfo> &)
{

}

void Editor::parse()
{

}

void Editor::render(col::Graphics& graphics)
{

}


