#pragma once

#include <string>
#include <set>
#include <deque>
#include <memory>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>


struct SfmlButton
{
	sf::Text sfText;

	SfmlButton(std::string text, const sf::Font& font, int size)
	{
		sfText.setFont(font);
		sfText.setString(text);
		sfText.setCharacterSize(24); // in pixels, not points!
		sfText.setFillColor(sf::Color::Red);
		sfText.setStyle(sf::Text::Bold | sf::Text::Underlined);
	}


	void load()
	{
		sf::Font font;
		if (!font.loadFromFile("arial.ttf"))
		{
			// error...
		}
	}

	//void render() {
	//	window.draw(sfText); // inside the main loop, between window.clear() and window.display()
	//}
};
