#pragma once
#include <cmath>
#include <array>
#include <type_traits>
#include <cstdint>

namespace Math {


template<typename T, size_t Size>
struct Vector;

template<typename A, typename B, size_t SizeA, size_t SizeB>
bool operator!=(const Vector<A, SizeA>&, const Vector<B, SizeB>&)
{
	return true;
}

template<typename T, size_t Size>
bool operator!=(const Vector<T, Size>& a, const Vector<T, Size>& b)
{
	for (size_t i = 0; i < Size; ++i)
	{
		if (a[i] != b[i])
			return true;
	}
	return false;
}

template<typename A, typename B, size_t SizeA, size_t SizeB>
bool operator==(const Vector<A, SizeA>& a, const Vector<B, SizeB>& b)
{
	return !(a != b);
}

#define Vector_Unary_Operator(op) \
template<typename A, size_t Size> \
auto operator op(const Vector<A, Size>& a) { \
	Vector<A, Size> result; \
	for (size_t i = 0; i < Size; ++i) \
		result[i] = op a[i]; \
	return result; \
}

Vector_Unary_Operator(-)
Vector_Unary_Operator(~)


#define Vector_Operator_Vector(op) \
template<typename A, typename B, size_t Size> \
auto operator op(const Vector<A, Size>& a, const Vector<B, Size>& b) \
{ \
	Vector<decltype(a[0] op b[0]), Size> result; \
	for (size_t i = 0; i < Size; ++i) \
		result[i] = a[i] op b[i]; \
	return result; \
} \
template<typename A, typename B, size_t Size> \
auto& operator op##= (Vector<A, Size>& a, const Vector<B, Size>& b) { \
	for (size_t i = 0; i < Size; ++i) \
		a[i] op##= b[i]; \
	return a; \
}


#define Vector_Operator_Scalar(op) \
template<typename A, typename B, size_t Size> \
auto operator op(const Vector<A, Size>& a, const B& b) \
{ \
	Vector<decltype(a[0] op b), Size> result; \
	for (size_t i = 0; i < Size; ++i) \
		result[i] = a[i] op b; \
	return result; \
} \
template<typename A, typename B, size_t Size> \
auto operator op(const A& a, const Vector<B, Size>& b) \
{ \
	Vector<decltype(a op b[0]), Size> result; \
	for (size_t i = 0; i < Size; ++i) \
		result[i] = a op b[i]; \
	return result; \
} \
template<typename A, typename B, size_t Size> \
auto& operator op##=(Vector<A, Size>& a, const B& b) \
{ \
	for (size_t i = 0; i < Size; ++i) \
		a[i] op##= b; \
	return a; \
}



Vector_Operator_Vector(+)
Vector_Operator_Vector(-)
Vector_Operator_Vector(*)
Vector_Operator_Vector(/)
Vector_Operator_Vector(%)
Vector_Operator_Vector(&)
Vector_Operator_Vector(|)
Vector_Operator_Vector(<<)
Vector_Operator_Vector(>>)

Vector_Operator_Scalar(+)
Vector_Operator_Scalar(-)
Vector_Operator_Scalar(*)
Vector_Operator_Scalar(/)
Vector_Operator_Scalar(%)
Vector_Operator_Scalar(&)
Vector_Operator_Scalar(|)
Vector_Operator_Scalar(<<)
Vector_Operator_Scalar(>>)



template<typename A, typename B, size_t Size>
auto dot(const Vector<A, Size>& a, const Vector<B, Size>& b)
{
	auto result = a[0] * b[0];
	for (size_t i = 1; i < Size; ++i)
		result += a[i] * b[i];
	return result;
}

template <typename A, typename B>
auto cross(const Vector<A, 3>& a, const Vector<B, 3>& b)
{
	return Vector<decltype(a[0] * b[0]), 3> {
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x
	};
}

template<typename A, size_t Size>
auto magnitudeSquared(const Vector<A, Size>& a)
{
	auto result = a[0] * a[0];
	for (std::size_t i = 1; i < Size; i++)
		result += a[i] * a[i];
	return result;
}

template<typename A, size_t Size>
auto magnitude(const Vector<A, Size>& a)
{
	return std::sqrt(magnitudeSquared(a));
}

template <typename A>
auto perpendicular(const Vector<A, 2>& a)
{
	return Vector<A, 2> {-a.y, a.x};
}

template<typename A, size_t Size>
[[nodiscard]] auto normalize(const Vector<A, Size>& a)
{
	return a / magnitude(a);
}

template <typename Vector3>
inline Vector3 rotateVecAroundVec(const Vector3& p, const Vector3& v, float phi)
{
	Vector3 AC(normalize(v) * dot(normalize(v), p) - p);
	if (magnitudeSquared(AC)<0.00001) return p;
	float AB = magnitude(AC)*cos((M_PI-phi)/2.0) * 2;

	float BD = AB * sin((M_PI-phi)/2.0);
	float AD = AB * cos((M_PI-phi)/2.0);

	return normalize(cross(v,p)) * BD + normalize(AC) * AD + p;
}


template <typename T, size_t Size>
struct VectorData : std::array<T, Size>
{
	const std::array<T, Size>& data() const {
		return *this;
	}

	std::array<T, Size>& data() {
		return *this;
	}
};

template <typename T>
struct VectorData<T, 2>
{
	T x;
	T y;

	const std::array<T, 2>& data() const {
		return *reinterpret_cast<const std::array<T, 2>*>(&x);
	}

	std::array<T, 2>& data() {
		return *reinterpret_cast<std::array<T, 2>*>(&x);
	}
};

template <typename T>
struct VectorData<T, 3>
{
	T x;
	T y;
	T z;

	const std::array<T, 3>& data() const {
		return *reinterpret_cast<const std::array<T, 3>*>(&x);
	}

	std::array<T, 3>& data() {
		return *reinterpret_cast<std::array<T, 3>*>(&x);
	}
};

template <typename T>
struct VectorData<T, 4>
{
	T x;
	T y;
	T z;
	T w;

	const std::array<T, 4>& data() const {
		return *reinterpret_cast<const std::array<T, 4>*>(&x);
	}

	std::array<T, 4>& data() {
		return *reinterpret_cast<std::array<T, 4>*>(&x);
	}
};


template<typename T, size_t Size>
struct Vector : VectorData<T, Size>
{
	static_assert(sizeof(VectorData<T, Size>) == sizeof(T) * Size);
	using Base = VectorData<T, Size>;

	const T& operator[](size_t index) const {
		return Base::data()[index];
	}

	T& operator[](size_t index) {
		return Base::data()[index];
	}

	template<int Offset, int SubvectorSize>
	auto subvector() -> Vector<T, SubvectorSize>& {
		static_assert(Offset + SubvectorSize <= Size);
		return *reinterpret_cast<Vector<T, SubvectorSize>*>(&(Base::data()[Offset]));
	}

	template<int Offset, int SubvectorSize>
	auto subvector() const -> const Vector<T, SubvectorSize>& {
		static_assert(Offset + SubvectorSize <= Size);
		return *reinterpret_cast<const Vector<T, SubvectorSize>*>(&(Base::data()[Offset]));
	}

	auto normalize() const
	{
		return Math::normalize(*this);
	}

	auto magnitude() const
	{
		return Math::magnitude(*this);
	}

	template <typename Target>
	Vector<Target, Size> staticCast() const
	{
		Vector<Target, Size> result;
		for (size_t i = 0; i < Size; ++i)
			result[i] = static_cast<Target>(Base::data()[i]);
		return result;
	}

	template <typename Target, typename std::enable_if_t<!std::is_convertible_v<T, Target>, bool> = false>
	explicit operator Vector<Target, Size>() const
	{
		return this->template staticCast<Target>();
	}

	template <typename Target, typename std::enable_if_t<std::is_convertible_v<T, Target>, bool> = false>
	operator Vector<Target, Size>() const
	{
		Vector<Target, Size> result;
		for (size_t i = 0; i < Size; ++i)
			result[i] = Base::data()[i];
		return result;
	}

	static auto one()
	{
		Vector<T, Size> result;
		for(int i = 0; i < Size ; ++i)
			result[i] = 1;
		return result;
	}
};


template <typename ...Args, typename T = std::common_type_t<Args...>>
Vector(Args...)->Vector<T, sizeof...(Args) >;




#define Vector234(t, type) \
using Vector2##t = Vector<type, 2>; \
using Vector3##t = Vector<type, 3>; \
using Vector4##t = Vector<type, 4>;

Vector234(b, bool)
Vector234(c, char)
Vector234(uc, unsigned char)
Vector234(s, short)
Vector234(us, unsigned short)
Vector234(i, int)
Vector234(ui, unsigned int)
Vector234(l, long)
Vector234(ul, unsigned long)
Vector234(ll, long long)
Vector234(ull, unsigned long long)
Vector234(f, float)
Vector234(d, double)

Vector234(i8, std::int8_t)
Vector234(ui8, std::uint8_t)
Vector234(i16, std::int16_t)
Vector234(ui16, std::uint16_t)
Vector234(i32, std::int32_t)
Vector234(ui32, std::uint32_t)
Vector234(i64, std::int64_t)
Vector234(ui64, std::uint64_t)


static_assert(sizeof(Vector2i) == sizeof(int) * 2);
static_assert(sizeof(Vector3d) == sizeof(double) * 3);
static_assert(sizeof(Vector4f) == sizeof(float) * 4);
static_assert(sizeof(Vector4s) == sizeof(short) * 4);
static_assert(sizeof(Vector3i8) == sizeof(std::int8_t) * 3);
static_assert(sizeof(Vector3ull) == sizeof(unsigned long long) * 3);



} // namespace
