#pragma once
#include "Math/Vector.h"

struct Place
{
	using vec3 = Math::Vector<float, 3>;

	vec3 r; // right
	vec3 u; // up
	vec3 f; // front
	vec3 p; // position
	vec3 s{1,1,1}; // scale

	void calcUp(); // By Front and Right
	void calcFront(); // By Up and Right

	void calcRight() {
		r = cross(f, u);
	}

	void turnUp(float phi) {
		f = normalize(rotateVecAroundVec(f, r, phi));
		u = normalize(cross(r, f)); //GetNormalized(&VecMultVecVect(&F, &R));
	};

	void turnDown(float phi) {
		using namespace Math;
		f = normalize(rotateVecAroundVec(f, r, -phi));
		u = normalize(cross(r, f)); //GetNormalized(&VecMultVecVect(&F, &R));
	};

	void turnLeft(float phi, vec3 up)
	{
		using namespace Math;
		f = normalize(rotateVecAroundVec(f, up,  phi));   // V
		u = normalize(rotateVecAroundVec(u, up,  phi));   // V
		r = normalize(cross(f, u)); //GetNormalized(&VecMultVecVect(&U, &F));
	}

	void turnLeft(float phi) {
		using namespace Math;
		f = normalize(rotateVecAroundVec(f, u,  phi));
		r = normalize(cross(f, u)); //GetNormalized(&VecMultVecVect(&U, &F));
	};

	void turnRight(float phi) {
		using namespace Math;
		f = normalize(rotateVecAroundVec(f, u, -phi));    // V
		r = normalize(cross(f, u)); // = GetNormalized(&VecMultVecVect(&U, &F));
	};

	void onward(float len) {
		using namespace Math;
		p += normalize(f) * len;
	}

	void backward(float len) {
		using namespace Math;
		p -= normalize(f) * len;
	}

	void strafeLeft(float len) {
		using namespace Math;
		p -= normalize(r) * len;
	}

	void strafeRight(float len) {
		using namespace Math;
		p += normalize(r) * len;
	}

	void strafeUp(float len) {
		using namespace Math;
		p += normalize(u) * len;
	}

	void strafeDown(float len) {
		using namespace Math;
		p-= normalize(u) * len;
	}

	template<typename Matrix4>
	Matrix4 worldMatrix() const {
		auto rScaled = r * s.x;
		auto uScaled = u * s.y;
		auto fScaled = f * s.z;
		return Matrix4(
			rScaled.x,  uScaled.x,  fScaled.x,  p.x,
			rScaled.y,  uScaled.y,  fScaled.y,  p.y,
			rScaled.z,  uScaled.z,  fScaled.z,  p.z,
			0, 0, 0, 1 );
	}

	template<typename Matrix4>
	Matrix4 worldMatrixTransposed() const {
		auto rScaled = r * s.x;
		auto uScaled = u * s.y;
		auto fScaled = f * s.z;
		return Matrix4(
			rScaled.x, rScaled.y, rScaled.z, 0,
			uScaled.x, uScaled.y, uScaled.z, 0,
			fScaled.x, fScaled.y, fScaled.z, 0,
			p.x, p.y, p.z, 1 );
	}

	template<typename Matrix4>
	Matrix4 viewMatrixTransposed() const {
		return Matrix4{
			 r.x,  u.x,  f.x, 0,
			 r.y,  u.y,  f.y, 0,
			 r.z,  u.z,  f.z, 0,
			-dot(r,p), -dot(u,p), -dot(f,p), 1 };
	}

	static auto identity()
	{
		return Place{ {1,0,0}, {0,1,0}, {0,0,1}, {0,0,0} };
	}
};



/*
auto axisRotation(vec3 vec, vec3 axis, float angle )
{
	vec3 result;

	// The axis is assumed to be normalized: (just make sure you're not modifying the original)
	axis = normalize(axis);

	// expanded for clarity:
	float u = axis.x;
	float v = axis.y;
	float w = axis.z;
	float x = vec.x;
	float y = vec.y;
	float z = vec.z;
	float c = cos(angle);
	float s = sin(angle);

	// Apply the formula verbatim from the linked page:
	result.x = u*(u*x + v*y + w*z)*(1.-c) + x*c + (-w*y + v*z)*s;
	result.y = v*(u*x + v*y + w*z)*(1.-c) + y*c + ( w*x - u*z)*s;
	result.z = w*(u*x + v*y + w*z)*(1.-c) + z*c + (-v*x + u*y)*s;

	return result;
}
*/

