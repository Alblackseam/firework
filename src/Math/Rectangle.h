#pragma once
#include "Vector.h"

namespace col
{
template<size_t Size, typename T>
struct Rectangle
{
	Math::Vector<T, Size> position;
	Math::Vector<T, Size> size;
};

using Rect2b = Rectangle<2, bool>;
using Rect3b = Rectangle<3, bool>;
using Rect4b = Rectangle<4, bool>;

using Rect2c = Rectangle<2, char>;
using Rect3c = Rectangle<3, char>;
using Rect4c = Rectangle<4, char>;

using Rect2i = Rectangle<2, int>;
using Rect3i = Rectangle<3, int>;
using Rect4i = Rectangle<4, int>;

using Rect2f = Rectangle<2, float>;
using Rect3f = Rectangle<3, float>;
using Rect4f = Rectangle<4, float>;

using Rect2d = Rectangle<2, double>;
using Rect3d = Rectangle<3, double>;
using Rect4d = Rectangle<4, double>;

} // namespace col

