#pragma once

#include "Math/Vector.h"
#include <string>
#include <set>
#include <deque>
#include <memory>

#include "Common.h"

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

struct Framebuffer
{
	unsigned int fbo;

	Framebuffer()
	{
		glGenFramebuffers(1, &fbo);
	}

	~Framebuffer()
	{
		glDeleteFramebuffers(1, &fbo);
	}

	void bind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	}

	static GLint getBinding()
	{
		GLint framebuffer;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &framebuffer);
		return framebuffer;
	}


	void unbind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0); // 0x8D40
		//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // 0x8CA9
	}

	void deleteFramebuffer()
	{
		glDeleteFramebuffers(1, &fbo);
	}

	void attachTexture(unsigned int texture)
	{
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 800, 600, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, texture, 0)

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
	}


	void renderbufferStorage(int width, int height)
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	}

};
