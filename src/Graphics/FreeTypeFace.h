#pragma once

#include <map>
#include <list>
#include <vector>
#include <memory>
#include <stdexcept>
#include <fstream>
#include "Common.h"
#include "Geometry/AVertexBuffer.h"
#include "Geometry/VertexTypes.h"

#include <ft2build.h>
#include FT_FREETYPE_H



struct FreeTypeLibrary
{
	FreeTypeLibrary()
	{
		if (FT_Init_FreeType(&ft))
			std::runtime_error("ERROR::FREETYPE: Could not init FreeType Library");
	}

	~FreeTypeLibrary()
	{
		FT_Done_FreeType(ft); // Завершение работы FreeType
	}

	FT_Library ft = nullptr;
};



struct Character
{
	GLuint     textureID; // ID текстуры глифа
	glm::ivec2 Size;      // Размеры глифа
	glm::ivec2 bitmapSize;
	glm::ivec2 Bearing;   // Смещение верхней левой точки глифа
	FT_Pos     Advance;   // Горизонтальное смещение до начала следующего глифа
};



struct FreeTypeFace
{
	FreeTypeFace(std::shared_ptr<FreeTypeLibrary> freeTypeLibrary, std::string name = "SourceCodePro-Regular.ttf")
		: freeTypeLibrary{freeTypeLibrary}
	{
		auto res = FT_New_Face(freeTypeLibrary->ft, name.c_str(), 0, &face);
		if (res)
			std::runtime_error("ERROR::FREETYPE: Failed to load font");
	}

	~FreeTypeFace()
	{
		FT_Done_Face(face);   // Завершение работы с шрифтом face
	}

	void setPixelSize(int height = 12, int width = 0)
	{
		if (FT_Set_Pixel_Sizes(face, width, height))
			std::runtime_error("FT_Set_Pixel_Sizes fails !");
	}

	FT_GlyphSlot getGlyphSlot(FT_ULong ch)
	{
		if (FT_Load_Char(face, ch, FT_LOAD_RENDER))
			std::runtime_error("ERROR::FREETYTPE: Failed to load Glyph");
		return face->glyph;
	}

protected:
	std::shared_ptr<FreeTypeLibrary> freeTypeLibrary;
	FT_Face face = nullptr;
	std::string name;
};





struct FreeTypeFaceRenderer : FreeTypeFace
{
	using VertexType = VertexPosUV;
	using VertexBuffer = AVertexBuffer<VertexType>;

	FreeTypeFaceRenderer(std::shared_ptr<FreeTypeLibrary> freeTypeLibrary, std::string name = "SourceCodePro-Regular.ttf")
		: FreeTypeFace { freeTypeLibrary, name }
		, vertexBuffer { std::make_shared<VertexBuffer>(4, GL_STATIC_DRAW) }
	{
		vertexBuffer->setNullData(4 * 10000); // TODO: ?
	}

	Character getCharacter(GLubyte c)
	{
		auto it = characters.find(c);
		if (it != characters.end())
			return it->second;
		else
			return makeCharacter(c);
	}

	std::shared_ptr<VertexBuffer> vertexBuffer;

protected:
	Character makeCharacter(GLubyte c)
	{
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

		auto* glyph = getGlyphSlot(c);

		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			glyph->bitmap.width,
			glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		Character character {
			texture,
			glm::ivec2(glyph->metrics.width, glyph->metrics.height),
			glm::ivec2(glyph->bitmap.width, glyph->bitmap.rows),
			glm::ivec2(glyph->bitmap_left, glyph->bitmap_top),
			glyph->advance.x
		};

		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		characters.emplace(c, character);
		return character;
	}

	std::map<GLchar, Character> characters;
};





struct TextRenderer
{
	std::shared_ptr<FreeTypeFaceRenderer> face;

	TextRenderer(std::shared_ptr<FreeTypeFaceRenderer> face)
		: face { face }
	{
	}

	void render(std::string text, GLfloat x, GLfloat y, GLfloat scale = 1.0f)
	{
		std::string::const_iterator c;
		for (c = text.begin(); c != text.end(); c++)
		{
			Character ch = face->getCharacter(*c);

			GLfloat xpos = x + ch.Bearing.x * scale;
			GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

			GLfloat w = ch.Size.x * scale;
			GLfloat h = ch.Size.y * scale;

			// Render glyph texture over quad
			glBindTexture(GL_TEXTURE_2D, ch.textureID);




			x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
		}
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	/*
	void renderText(Shader &s, std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color)
	{
		// Activate corresponding render state
		s.Use();
		glUniform3f(glGetUniformLocation(s.Program, "textColor"), color.x, color.y, color.z);
		glActiveTexture(GL_TEXTURE0);
		glBindVertexArray(VAO);

		// Iterate through all characters
		std::string::const_iterator c;
		for (c = text.begin(); c != text.end(); c++)
		{

			Character ch = face->getCha Characters[*c];

			GLfloat xpos = x + ch.Bearing.x * scale;
			GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * scale;

			GLfloat w = ch.Size.x * scale;
			GLfloat h = ch.Size.y * scale;
			// Update VBO for each character
			GLfloat vertices[6][4] = {
				{ xpos    , ypos + h, 0.0, 0.0 },
				{ xpos    , ypos    , 0.0, 1.0 },
				{ xpos + w, ypos    , 1.0, 1.0 },
				{ xpos    , ypos + h, 0.0, 0.0 },
				{ xpos + w, ypos    , 1.0, 1.0 },
				{ xpos + w, ypos + h, 1.0, 0.0 }
			};
			// Render glyph texture over quad
			glBindTexture(GL_TEXTURE_2D, ch.textureID);
			// Update content of VBO memory
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			// Render quad
			glDrawArrays(GL_TRIANGLES, 0, 6);
			// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
			x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
		}
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	*/


};


//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

//RenderText(shader, "This is sample text", 25.0f, 25.0f, 1.0f, glm::vec3(0.5, 0.8f, 0.2f));
//RenderText(shader, "(C) LearnOpenGL.com", 540.0f, 570.0f, 0.5f, glm::vec3(0.3, 0.7f, 0.9f));


