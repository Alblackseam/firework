#pragma once

#include <string>
#include <set>
#include <deque>
#include <memory>
#include <functional>
#include <iostream>

#include "Common.h"
#include "Graphics.h"
#include "Math/Place.h"
#include "Math/Rectangle.h"


namespace col
{

struct ButtonDesc
{
	Rect2f rect;
	std::function<void()> handler;
	Math::Vector4f color;
	unsigned int textureId {};
};


}

