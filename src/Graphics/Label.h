#pragma once

#include <string>
#include <set>
#include <deque>
#include <memory>
#include <functional>
#include <iostream>

#include "Common.h"
#include "Graphics/Graphics.h"
#include "Math/Place.h"
#include "Geometry/Primitives.h"
#include "Geometry/AVertexBuffer.h"
#include "Graphics/CameraHelpers.h"



#include "FreeTypeFace.h"

namespace col
{

inline void rectangleRender(float x, float y, float w, float h)
{
	glBegin(GL_TRIANGLE_STRIP);

	glTexCoord2f(0, 0);
	glVertex3f(x, y, 0);

	glTexCoord2f(0, 1);
	glVertex3f(x, y + h, 0);

	glTexCoord2f(1, 0);
	glVertex3f(x + w, y, 0);

	glTexCoord2f(1, 1);
	glVertex3f(x + w, y + h, 0);

	glEnd();
}



template<bool UseViewMatrix = true>
struct Label
{
	using Form = Square;
	//using VertexBuffer = FreeTypeFaceRenderer::VertexBuffer;
	using VertexType = FreeTypeFaceRenderer::VertexType;

	struct Command
	{
		size_t startVertexIndex = 0;
		size_t vertexCount = 0;
		GLuint textureId = 0;
	};

	void setText(std::string _text)
	{
		if (text == _text)
			return;

		text = _text;
		bound = {};
		for (auto& c : text)
		{
			auto ci = freeTypeFaceRenderer->getCharacter(c);
			bound.y = std::max(ci.Size.y, bound.y);
			bound.x += ci.Advance;
		}

		bound.x >>= (6);
		bound.y >>= (6);

		commands.clear();
	}


	void render(col::Graphics& graphics, int texturedShader)
	{
		auto offset = glGetUniformLocation(texturedShader, "color");
		glUniform4fv(offset, 1, color.data().data());


		auto view = UseViewMatrix ? graphics.view * place.worldMatrixTransposed<glm::mat4x4>() : place.worldMatrixTransposed<glm::mat4x4>();
		//view = glm::scale(view, glm::vec3{sx, sy, 1});

		auto viewMatrixOffset = glGetUniformLocation(texturedShader, "viewMatrix");
		glUniformMatrix4fv(viewMatrixOffset,  1, GL_FALSE, glm::value_ptr(view));

		if (commands.empty())
			makeCommands();

		for (auto& command : commands)
		{
			freeTypeFaceRenderer->vertexBuffer->bind();
			VertexType::enableAttribArrays();
			VertexType::setAttribPointers();

			glBindTexture(GL_TEXTURE_2D, command.textureId);
			glDrawArrays(GL_TRIANGLE_STRIP, command.startVertexIndex, command.vertexCount);

			VertexType::disableAttribArrays();
			freeTypeFaceRenderer->vertexBuffer->unbind();
		}
	}

	std::string text;
	glm::ivec2 bound {};


	Place place{{1,0,0}, {0,1,0}, {0,0,1}};
	std::function<void()> handlerLeftButton;
	Math::Vector4f color{1,1,1, 0.9f};

	std::shared_ptr<FreeTypeFaceRenderer> freeTypeFaceRenderer;

protected:
	void makeCommands()
	{
		GLfloat x = -0.5f;
		GLfloat y = -0.5f;

		float scaleX = 1.0f / bound.x;
		float scaleY = 1.0f / bound.y;
		auto minScale = std::min(scaleX, scaleY);
		scaleX = scaleY = minScale;

		for (auto& c : text)
		{
			auto ci = freeTypeFaceRenderer->getCharacter(c);

			GLfloat xpos = x + ci.Bearing.x * scaleX;
			GLfloat ypos = y - (ci.bitmapSize.y - ci.Bearing.y) * scaleY;

			GLfloat w = ci.bitmapSize.x * scaleX;
			GLfloat h = ci.bitmapSize.y * scaleY;

			std::vector<VertexType> vertices;

			vertices.push_back(VertexType{{x    , y + h, 0, 1}, {0, 0}});
			vertices.push_back(VertexType{{x    ,     y, 0, 1}, {0, 1}});
			vertices.push_back(VertexType{{x + w, y + h, 0, 1}, {1, 0}});
			vertices.push_back(VertexType{{x + w,     y, 0, 1}, {1, 1}});

			auto startVertexIndex = freeTypeFaceRenderer->vertexBuffer->vertexCount;
			auto vertexCount = vertices.size();
			freeTypeFaceRenderer->vertexBuffer->appendData(vertices);

			commands.emplace_back(std::move(Command{ startVertexIndex, vertexCount, ci.textureID }));

			x += (ci.Advance >> 6) * scaleX; // Bitshift by 6 to get value in pixels (2^6 = 64)
		}
	}

	std::vector<Command> commands;
};



}

