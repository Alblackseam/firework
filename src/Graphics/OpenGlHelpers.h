#pragma once

#include <string>
#include "Common.h"


std::string readFile(std::string name);

GLint checkShaderForErrors(GLuint shaderID, std::string errorPrefix);

GLint checkProgramForErrors(GLuint programID, std::string errorPrefix);

GLuint loadComputeShader(std::string computeShaderFileName);

GLuint loadShaders(std::string vertexShaderFileName, std::string fragmentShaderFileName);

void renderCoordSystem();

