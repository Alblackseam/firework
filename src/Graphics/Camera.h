#pragma once
#include "Math/Place.h"
#include "Common.h"

struct Camera
{
	using vec3 = Math::Vector<float, 3>;

	float fov;
	float zNear;
	float zFar;
	Place place;

	// resolution
	float height;
	float width;

	float fovDegrees() const {
		return fov * 180 / M_PI;
	}

	float getAspect() const {
		return width / height;
	}

	void rotateAroundVector(const vec3& v, float a) {
		place.f = Math::rotateVecAroundVec(place.f, v, a);
		place.r = Math::rotateVecAroundVec(place.r, v, a);
		place.u = Math::rotateVecAroundVec(place.u, v, a);
	}

	Math::Vector2f viewHalfSizeOnNearPlane() const {
		auto heightOnNear = tanf(fov / 2) * zNear;
		return {heightOnNear * width / height, heightOnNear};
	}
};

