#pragma once
#include "Math/Place.h"
#include "Common.h"


namespace col {

template <typename T>
static T fromProc(T proc)
{
	return proc * 0.01f;
}


inline Place calculatePlaceNearPlane(const Math::Vector2f & viewHalfSizeOnNearPlane, float cameraZnear, const col::Rect2f & rect)
{
	auto scrHalfSize = viewHalfSizeOnNearPlane;
	auto scrSize = scrHalfSize * 2;

	auto sz = scrHalfSize * fromProc(rect.size);
	auto pos = scrSize * fromProc(rect.position) - scrHalfSize + sz;
	pos.y *= -1;

	Place p{ {1,0,0}, {0,1,0}, {0,0,1}, {pos.x, pos.y, -cameraZnear - 0.001f}};
	p.s = {sz.x, sz.y, 1};

	return p;
}

} // namespace col

