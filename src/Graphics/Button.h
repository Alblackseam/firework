#pragma once

#include <string>
#include <set>
#include <deque>
#include <memory>
#include <functional>
#include <iostream>

#include "Common.h"
#include "Graphics.h"
#include "Math/Place.h"
#include "ButtonDesc.h"
#include "Geometry/Primitives.h"


namespace col
{


template<typename Form, bool UseViewMatrix = true>
struct Button
{
	void render(col::Graphics& graphics)
	{
		// glGetUniformLocation(texturedShader, "color")
		glUniform4fv(0, 1, color.data().data());

		glMatrixMode(GL_MODELVIEW);

		if constexpr(UseViewMatrix)
		{
			glLoadMatrixf(glm::value_ptr(graphics.view * place.worldMatrixTransposed<glm::mat4x4>()));//glLoadMatrixf(glm::value_ptr(graphics.view * transform));
		}
		else {
			glLoadMatrixf(glm::value_ptr(place.worldMatrixTransposed<glm::mat4x4>()));
		}

		glBindTexture(GL_TEXTURE_2D, desc.textureId);
		Form::render();
	}

//	void onLeftMouseButtonReleased(float x, float y)
//	{
//		auto pos = glm::inverse(place.template worldMatrixTransposed<glm::mat4x4>()) * glm::vec4{x, y, 0, 1};
//		if (handlerLeftButton)
//			if (form.isInside(pos.x, pos.y))
//				handlerLeftButton();
//	}

	Form form;
	Place place{{1,0,0}, {0,1,0}, {0,0,1}};
	std::function<void()> handlerLeftButton;
	Math::Vector4f color{1,1,1,1};

	ButtonDesc desc;
};


template<typename Button>
void onLeftMouseButtonReleased(const glm::vec2& pos, Button& button)
{
	using namespace std;

	glm::mat4 invTransform = glm::inverse(button.place.template worldMatrixTransposed<glm::mat4x4>());
	auto posLocal = invTransform * glm::vec4{pos, 0, 1}; //std::cout << posLocal.x << ", " << posLocal.y << std::endl;
	//cout << "posLocal: " << posLocal.x << ", " << posLocal.y << endl;
	//cout << zNearPlaneCoords.x << ", " << zNearPlaneCoords.y << endl;

	if (button.handlerLeftButton)
		if (button.form.isInside(posLocal.x, posLocal.y))
			button.handlerLeftButton();
}

template<typename Button>
void onLeftMouseButtonReleased(const glm::vec3& pos, Button& button, col::Graphics& graphics)
{
	auto posLocal = graphics.unproject2(pos, button.place.template worldMatrixTransposed<glm::mat4x4>());
	if (button.handlerLeftButton)
		if (button.form.isInside(posLocal.x, posLocal.y))
			button.handlerLeftButton();
}


}

