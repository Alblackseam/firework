#pragma once

#include <string>
#include <set>
#include <deque>
#include <memory>
#include "Common.h"
#include "Math/Vector.h"

template<typename T>
glm::vec<3, T> toGlm(const Math::Vector<T, 3>& v) {
	return glm::vec<3, T>{v.x, v.y, v.z};
}

template<typename T>
glm::vec<3, T> toGlm(const glm::vec<3, T>& v) {
	return v;
}

template<typename T>
Math::Vector<T, 3> toMath(const glm::vec<3, T>& v) {
	return Math::Vector<T, 3>{v.x, v.y, v.z};
}



namespace col
{

struct Graphics
{
	glm::mat4x4 proj;
	glm::mat4x4 view;
	glm::ivec4 viewport;

//	glm::vec3 getRayFromMouse(const glm::vec2& mousePos)
//	{
//		glm::vec4 ray_clip = glm::vec4(mousePos.x, mousePos.y, 1.0f, 1.0f);
//		glm::vec4 eyeCoords = glm::inverse(proj) * ray_clip;
//		eyeCoords = glm::vec4(eyeCoords.x, eyeCoords.y, 1.0f, 0.0f); // -1.0f
//		glm::vec4 rayWorld = glm::inverse(view) * eyeCoords;
//		glm::vec3 rayDirection = glm::normalize(glm::vec3(rayWorld));
//		return rayDirection;
//	}


	// https://coderedirect.com/questions/390462/how-to-calculate-look-at-point-to-move-the-camera-with-the-mouse-in-opengl-glut
//	glm::vec3 Unproject( const glm::vec3& win )
//	{
//		glm::mat4 proj, model;
//		glGetFloatv(GL_MODELVIEW_MATRIX, &model[0][0]);
//		glGetFloatv(GL_PROJECTION_MATRIX, &proj[0][0]);

//		glm::ivec4 viewport;
//		glGetIntegerv(GL_VIEWPORT, &viewport[0]);

//		glm::vec3 world = glm::unProject( win, model, proj, viewport );
//		return world;
//	}

	glm::vec3 Unproject( const glm::vec3& win, const glm::mat4& proj, const glm::mat4& model, const glm::ivec4& viewport )
	{
		return glm::unProject( win, model, proj, viewport ); // glm::vec3 world
	}

	// unprojects the given window point and finds the ray intersection with the Z=0 plane
	template<typename Vector2>
	glm::vec2 PlaneUnproject( const Vector2& win )
	{
		glGetIntegerv(GL_VIEWPORT, &viewport[0]);

		glm::vec3 world1 = Unproject( glm::vec3( win.x, win.y, 0.01f ), proj, view, viewport );
		glm::vec3 world2 = Unproject( glm::vec3( win.x, win.y, 0.99f ), proj, view, viewport );

		// u is a value such that:
		// 0 = world1.z + u * ( world2.z - world1.z )
		float u = -world1.z / ( world2.z - world1.z );
		// clamp u to reasonable values
		if( u < 0 ) u = 0;
		if( u > 1 ) u = 1;

		return glm::vec2( world1 + u * ( world2 - world1 ) );
	}


	template<typename Vector3>
	glm::vec3 unproject2( const Vector3& win, const glm::mat4x4& view )
	{
		glGetIntegerv(GL_VIEWPORT, &viewport[0]);

		glm::vec3 world = Unproject( toGlm( win ), proj, view, viewport );
		return world;
	}
};

}

