#include <fstream>
#include <iostream>
#include "OpenGlHelpers.h"
#include "utility/StringUtils.h"

// http://shdr.bkcore.com/#1/nVI9b9swEP0rBy2VYkK223hx0HYoUKBDi0wdWhcBI1IOA/pOICk7duD/3iMpua7TLJ1E3ce7997dc6Go6Tcagy+WP4vO6cZ4QwgPZv3QQWtJhpsVbqXbG1zDVjfvoP1GbiPtZXhHzqpb8pdxS420Ob7CBAhdb70u83srrchzQPlQwfMKAZwOvcMYJlduSJVcdcVpMa9n1aReVAx2jHhbMgo20mBZrTC1ppnKOHifnuVMzMWsuoHpNGkCJMJYd6ptOvJcfEYzJxtCH0ZmXLCoZ0kCRCikoJfwBRThG04T/ABvsNFgAlgtlYdAsNPGKehkCNqhj50Z7d4xkcCQ2YdIoH4SqoLJeWTPkWFg5kmWoqpoR0YQb9kN+AD1Aj5msfMkFpZ/pM9ZegYZlJi27b1mHO6aMPNQDvsUbFqV1K/t3Wcn15+GgYx1XY59V5mHgLiJcQuFePVyWLwz933QWQQLM4GrXmZwPKoeTctv3moYo18llz79nbuORmj73ejdv9Odo0fdxGGn/Ctn/D+H/PLwBjw2LJM2B12e02fr8m82OboK+fQuhHBhsnz06uQ1nKjF24lHsz+k8Mgsh0eD0yJvh/+YujCE53RJzbH4JYpEoljmr5/6/iAR9V3cZv3oi+Nv

std::string readFile(std::string name)
{
	std::ifstream t(name);
	t.seekg(0, std::ios::end);
	auto size = t.tellg();
	std::string buffer(size, ' ');
	t.seekg(0);
	t.read(buffer.data(), size);
	return buffer;
}

GLint checkShaderForErrors(GLuint shaderID, std::string errorPrefix)
{
	GLint result = GL_FALSE;
	int infoLogLength {};

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0)
	{
		char* shaderErrorMessage = new char[infoLogLength];
		glGetShaderInfoLog(shaderID, infoLogLength, NULL, &(shaderErrorMessage[0]));
		auto error = errorPrefix + ": " + shaderErrorMessage;
		std::cerr << error << std::endl;
		throw std::runtime_error(error);
	}
	return result;
}

GLint checkProgramForErrors(GLuint programID, std::string errorPrefix)
{
	GLint result = GL_FALSE;
	int infoLogLength {};

	glGetProgramiv(programID, GL_LINK_STATUS, &result);
	glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0)
	{
		char * programErrorMessage = new char[infoLogLength];
		glGetProgramInfoLog(programID, infoLogLength, NULL, &(programErrorMessage[0]));
		auto error = errorPrefix + ": " + programErrorMessage;
		std::cerr << error << std::endl;
		throw std::runtime_error(error);
	}
	return result;
}


GLuint loadComputeShader(std::string computeShaderFileName)
{
	GLuint computeShaderID = glCreateShader(GL_COMPUTE_SHADER);
	auto shaderSource = readFile(computeShaderFileName);
	const GLchar* sources[1] = {&shaderSource[0]};

	glShaderSource(computeShaderID, 1, sources, NULL);
	glCompileShader(computeShaderID);

	checkShaderForErrors(computeShaderID, "glCompileShader failed:");

	GLuint programID = glCreateProgram();
	glAttachShader(programID, computeShaderID);
	glLinkProgram(programID);

	checkProgramForErrors(programID, "glLinkProgram failed: ");
	glDeleteShader(computeShaderID);

	std::cout << "Compute Shader Program ID: " << programID << std::endl;
	return programID;
}



GLuint loadShaders(std::string vertexShaderFileName, std::string fragmentShaderFileName)
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	auto vertexShaderSource = readFile(vertexShaderFileName);
	const GLchar* sources [1] = {&vertexShaderSource[0]};

	glShaderSource(vertexShaderID, 1, sources, NULL);
	glCompileShader(vertexShaderID);

	checkShaderForErrors(vertexShaderID, "glCompileShader failed:");

	auto fragmentShaderSource = readFile(fragmentShaderFileName);
	const GLchar* fragSources [1] = {&fragmentShaderSource[0]};

	glShaderSource(fragmentShaderID, 1, fragSources , NULL);
	glCompileShader(fragmentShaderID);

	checkShaderForErrors(fragmentShaderID, "fragmet shader compile faild:");

	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, vertexShaderID);
	glAttachShader(ProgramID, fragmentShaderID);

	glLinkProgram(ProgramID);

	checkProgramForErrors(ProgramID, "link program failed:");

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);
	return ProgramID;
}

void renderCoordSystem()
{
	glBegin(GL_LINES);
		glColor4ub(255,255,255,255);
		glVertex3f(0, 0, 0);
		glColor4ub(255,0,0,255);
		glVertex3f(1, 0, 0);
		glColor3ub(255,255,255);
		glVertex3f(0, 0, 0);
		glColor3ub(0, 255, 0);
		glVertex3f(0, 1, 0);
		glColor3ub(255,255,255);
		glVertex3f(0, 0, 0);
		glColor3ub(0, 0, 255);
		glVertex3f(0, 0, 1);
	glEnd();
}

