#pragma once

#include "Math/Vector.h"
#include <string>
#include <set>
#include <deque>
#include <memory>

#include "Common.h"

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>


struct Texture
{
	unsigned int id {0};
	int width;
	int height;

	Texture(int width, int height)
		: width{width}
		, height{height}
	{
		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);

		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, NULL);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

};

