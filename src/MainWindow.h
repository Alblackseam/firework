#pragma once

#include <iostream>
#include <fstream>
#include "utility/BinaryStream.h"
#include "utility/DebugUtils.h"
#include <string>
#include <tuple>
#include <thread>
#include <map>
#include <set>
#include <sstream>
#include <optional>
#include <SFML/Graphics.hpp>

struct MainWindow
{
	int width = 2200;
	int height = 1200;

	sf::View view;
	sf::RenderWindow window;

	std::optional<sf::Vector2i> catchPoint;
	std::vector<sf::VertexBuffer> vertexBuffers;
	std::vector<sf::Text> texts;
	std::vector<sf::Text> gridTexts;
	sf::Font font;
	sf::VertexBuffer gridVb{ sf::PrimitiveType::Lines, sf::VertexBuffer::Dynamic };


	MainWindow()
		: window{ sf::VideoMode(width, height), "Window", sf::Style::Default }
	{
	}


	void makeGridText(sf::Font& font, std::string text, sf::Vector2f point)
	{
		sf::Text textLabel(text, font);
		textLabel.setCharacterSize(12);
		//textLabel.setStyle(sf::Text::Bold);
		textLabel.setFillColor(sf::Color::White);
		textLabel.setPosition(point);
		gridTexts.push_back(textLabel);
	}

	void drawGrid(const sf::Vector2f& upLeft, const sf::Vector2f& downRight)
	{
		using namespace sf;

		auto leftMs = static_cast<int>(upLeft.x);
		auto rightMs = static_cast<int>(downRight.x);

		leftMs = leftMs / 100 * 100;
		rightMs = rightMs / 100 * 100;

		auto length = rightMs - leftMs;

		auto linesCount = static_cast<int>(length) / 100;
		std::vector<Vertex> vertices;
		vertices.resize(linesCount * 2);
		gridTexts.clear();

		for (int i = 0; i < linesCount; ++i)
		{
			auto time = leftMs + i * 100;
			auto timeF = static_cast<float>(time);

			vertices[i * 2 + 0] = Vertex{ Vector2f{timeF, 10}, sf::Color{128,128,128,128} };
			vertices[i * 2 + 1] = Vertex{ Vector2f{timeF, 50}, sf::Color{128,128,128,128} };

			auto seconds = time / 1000;
			makeGridText(font, std::to_string(timeF / 1000), {timeF, 50});
		}


		if (vertices.size() != 0)
			if (vertices.size() != gridVb.getVertexCount())
				gridVb.create(vertices.size());
		gridVb.update(vertices.data());
	}

	void init()
	{
		using namespace sf;
		window.setPosition(Vector2i(50, 100));

		//auto videoModes = sf::VideoMode::getFullscreenModes();
		//auto currentVm = videoModes[2];
		//window.create(currentVm, "Window", sf::Style::Fullscreen);


		view.reset(sf::FloatRect(0, 0, width, height));
		view.setViewport(sf::FloatRect(0, 0, 1, 1));
		window.setView(view);

		font.loadFromFile("SourceCodePro-Regular.ttf");
	}


	int run()
	{
		using namespace sf;

		while (window.isOpen())
		{
			sf::Event event;
			while (window.pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::Closed:
					window.close();
					break;

				case sf::Event::KeyPressed:
					if (event.key.code == Keyboard::Escape)
						window.close();
					break;

				case sf::Event::MouseButtonPressed:
					if (event.mouseButton.button == Mouse::Button::Left)
						catchPoint = { event.mouseButton.x, event.mouseButton.y };
					break;

				case sf::Event::MouseButtonReleased:
					catchPoint.reset();
					if (event.mouseButton.button == Mouse::Button::Right)
					{
						sf::Vector2i pointScreen{ event.mouseButton.x, event.mouseButton.y };

						auto point = window.mapPixelToCoords(pointScreen, view);
						//cout << point.x << ',' << point.y << endl;
					}
					break;

				case sf::Event::MouseMoved:
					if (catchPoint)
					{
						//cout << event.mouseButton.button << "," << Mouse::Button::Left << endl;
						auto newPoint = Vector2i{ event.mouseMove.x, event.mouseMove.y };
						if (newPoint != *catchPoint)
						{
							auto delta = window.mapPixelToCoords(newPoint, view) - window.mapPixelToCoords(*catchPoint, view);
							view.move(-static_cast<sf::Vector2f>(delta));
							catchPoint = newPoint;

							drawGrid(window.mapPixelToCoords({ 0, 0 }, view), window.mapPixelToCoords({ width, height }, view));
						}
					}
					break;

				case sf::Event::MouseWheelScrolled:
					if (event.mouseWheelScroll.delta > 0)
						view.zoom(1.1f);
					else
						view.zoom(0.9f);
					drawGrid(window.mapPixelToCoords({ 0, 0 }, view), window.mapPixelToCoords({ width, height }, view));
					break;

				default:
					break;
				}
			}

			//window.clear(Color{250, 250, 250, 255});
			window.clear();

			window.setView(view);
			for (const auto& vertexBuffer : vertexBuffers)
				window.draw(vertexBuffer);

			if (gridVb.getVertexCount() > 0)
				window.draw(gridVb);

			for (const auto& text : gridTexts)
				window.draw(text);

			window.setView(window.getDefaultView());

			for (const auto& text : texts)
				window.draw(text);

			window.display();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		return 0;
	}
};




