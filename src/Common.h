#pragma once

#if defined(WIN32)
	#include <Windows.h>
	#include <windows.h>
	#include <GL/glew.h>
#else
	#define GL_GLEXT_PROTOTYPES
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>

