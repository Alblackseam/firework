#include "Vector.h"
#include "Game/Scene.h"
#include "Place.h"
#include "Editor.h"
#include "Game/Game.h"

#include <vector>
#include <cmath>
#include <iostream>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window.hpp>
#include "SFML/Graphics.hpp"
#include <SFML/OpenGL.hpp>
#include <thread>

int main()
{
	using namespace sf;
#if defined (WIN32)
	glewExperimental = GL_TRUE;
#endif

	int width = 1280;
	int height = 1024;
	sf::RenderWindow window {
		sf::VideoMode(width, height),
		"Window",
		sf::Style::Default,
		sf::ContextSettings {
			24, // depth
			8, // Request a 8 bits stencil buffer
			4 // Request 4 levels of antialiasing
		}
	};
	window.setVerticalSyncEnabled(true);

#if defined (WIN32)
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		auto s = glewGetErrorString(err);
		std::cout << s;
	}
#endif


	Scene scene{ window };
	scene.window.pushGLStates();
	scene.init();
	scene.window.popGLStates();

	//Editor control{scene};
	Game control{scene};
	control.init();

	sf::Color bgColor{ 0, 0, 32 };
	scene.window.setTitle("Colonizators");

	sf::Clock deltaClock;
	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			case sf::Event::KeyPressed:
				if (scene.onKeyPressed(event.key))
					continue;
				break;

			case sf::Event::KeyReleased:
				if (scene.onKeyReleased(event.key))
					continue;
				break;

			case sf::Event::Resized:
				scene.onResize(event.size.width, event.size.height);
				//editor.init();
				break;

			case sf::Event::MouseMoved:
				scene.onMouseMoved(event.mouseMove);
				break;

			case sf::Event::MouseButtonPressed:
				scene.onMouseButtonPressed(event.mouseButton);
				break;

			case sf::Event::MouseButtonReleased:
				scene.onMouseButtonReleased(event.mouseButton);
				break;

			default:
				break;
			}
		}

		scene.update(deltaClock.getElapsedTime().asSeconds());
		//window.clear(bgColor);
		scene.clear();
		scene.render([&control](col::Graphics& gr){
				control.render(gr);
			});

		deltaClock.restart();

		window.display();
		std::this_thread::sleep_for(std::chrono::milliseconds(7));
	}

	return 0;
}
