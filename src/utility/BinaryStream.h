#pragma once

#include <map>
#include <array>
#include <cassert>
#include <chrono>
#include <cstdint>
#include <cstdio>
#include <deque>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cstdint>

#if __has_include(<endian.h>)
#include <endian.h>
#endif


#if _BYTE_ORDER != _LITTLE_ENDIAN || defined (__BIG_ENDIAN_BITFIELD)
#  error "Big-Endian Arch is not supported"
#endif

namespace BinaryStream {


template<int MaxSize, typename T>
std::vector<uint8_t> vlqEncodeInternal(T x)
{
	int i = MaxSize;
	for (; i > 0; i--) {
		if (x & 127ULL << i * 7) {
			break;
		}
	}

	std::vector<uint8_t> out;
	for (int j = 0; j <= i; j++) {
		out.push_back(((x >> ((i - j) * 7)) & 127) | 128);
	}
	out[i] ^= 128;
	return out;
}

inline std::vector<uint8_t> vlqEncode(std::uint32_t x) {
	return vlqEncodeInternal<5>(x);
}

inline std::vector<uint8_t> vlqEncode(std::uint64_t x) {
	return vlqEncodeInternal<9>(x);
}


template <typename Carrier = std::istream>
inline uint64_t decodeVlq(Carrier& sm) {
    uint64_t r{};
    unsigned char input{};
    do
    {
		sm.read(reinterpret_cast<char*>(&input), 1); //sm >> input;
		r = (r << 7) | (input & 127);
    } while (input & 0x80);
    return r;
}


template <typename Carrier = std::ostream>
struct Writer
{
    Writer(std::ostream& sm)
        : sm{sm}
    {
    }

	Writer& operator << (const uint32_t& value) {
		auto buffer = vlqEncode(value);
		sm.write(reinterpret_cast<const char*>(buffer.data()), buffer.size());
		return *this;
	}

    Writer& operator << (const uint64_t& value) {
		auto buffer = vlqEncode(value);
		sm.write(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        return *this;
    }

    Writer& operator << (const std::string& value) {
        operator << (static_cast<uint64_t>(value.size()));
        sm.write(value.data(), value.size());
        return *this;
    }

protected:
    Carrier& sm;
};


template <typename Carrier = std::istream>
struct Reader
{
    Reader(std::istream& sm)
            : sm{sm}
    {
    }

    Reader& operator >> (uint64_t& value) {
        value = decodeVlq(sm);
        return *this;
    }

    Reader& operator >> (std::string& value) {
        uint64_t size{};
        *this >> size;
        value.resize(size);
        sm.read(value.data(), size);
        return *this;
    }

protected:
    Carrier& sm;
};

}

