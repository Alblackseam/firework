#pragma once

#include <map>
#include <array>
#include <cassert>
#include <chrono>
#include <cstdint>
#include <cstdio>
#include <deque>
#include <fstream>
#include <string>
#include <string_view>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include "StringNarrow.h"

#if __has_include("utility/BinaryStream.h")
#include "utility/BinaryStream.h"
#endif


#if defined(__ANDROID__)

constexpr auto debugOutputDir = "/storage/emulated/0"; // std::string_view{"/storage/emulated/0"};
#define THIS_FUNCTION_NAME __PRETTY_FUNCTION__

#elif defined(WIN32)

constexpr auto debugOutputDir = std::string_view{ "D:/" };
#if (_DEBUG == 1)
#define DEBUG_BUILD 1
#endif

#define THIS_FUNCTION_NAME __FUNCSIG__


#endif


#define THIS_PLACE (__FILE__ + std::string(": ") + std::to_string(__LINE__))
#define WHICH(arg) #arg << ":" << arg
#define STRING_EXPAND(tok) #tok
#define STRINGIFY(tok) STRING_EXPAND(tok)

#define AUTO_SECTION Debug::AutoSection section{THIS_FUNCTION_NAME}



namespace Debug {

    struct Section {
        using Clock = std::chrono::steady_clock;

        Section() = default;

        Section(const Clock::time_point &managerCreateTime, bool doFixTime = false)
                : managerCreateTime{managerCreateTime}, lastFixTime{0} {
            if (doFixTime)
                fixTime();
        }

        void fixTime() {
            auto now = Clock::now();
            auto microsecondsSinceCreate = static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::microseconds>(
                    now - managerCreateTime).count());
            measurements.push_back(microsecondsSinceCreate - lastFixTime);
            lastFixTime = microsecondsSinceCreate;
        }

        std::string toString() const {
            std::string result;
            for (const auto &m : measurements) {
                result += std::to_string(m) + ", ";
            }
            return result;
        }

        std::deque<uint64_t> measurements;

    protected:
        Clock::time_point managerCreateTime;
        uint64_t lastFixTime;
    };


    template<typename Stream>
    inline Stream &operator<<(Stream &sm, const Section &section) {
        sm << static_cast<uint64_t>(section.measurements.size());
        for (const auto &m : section.measurements)
            sm << m;
        return sm;
    }

    template<typename Stream>
    inline Stream& operator>>(Stream& sm, Section& section) {
        uint64_t measurementsSize{};
        sm >> measurementsSize;
        for (uint64_t i{0}; i < measurementsSize; ++i) //  const auto& m : )
        {
            uint64_t m;
            sm >> m;
            section.measurements.push_back(m);
        }
        return sm;
    }


    struct DummySectionManager {
        void start() {}

        void stop() {}
    };


    struct SectionManager {
        void start(std::string name) {
            auto it = sections.find(name);
            if (it != sections.end()) {
                it->second.fixTime();
            } else {
                sections.insert(std::pair<std::string, Section>{name, Section{managerCreateTime, true}});
            }
        }

        void stop(std::string name) {
            auto it = sections.find(name);
            assert(it != sections.end());
            it->second.fixTime();
        }

        std::string info() const {
            std::string result;
            for (const auto&[k, v] : sections) {
                result += k + ": " + v.toString() + "\n";
            }
            return result;
        }

        void writeToFile(const std::string& fileName = "c_diary_sections.dat") const {
            std::array<char, 100000> buffer;
            std::ofstream out;
            out.rdbuf()->pubsetbuf(buffer.data(), buffer.size());
            //out.open(std::string{debugOutputDir} + "/" + fileName, std::ios_base::binary);

#if __has_include("utility/BinaryStream.h")
            BinaryStream::Writer sm{out};
#else
            auto & sm = out;
#endif
            sm << static_cast<uint64_t>(sections.size());
            for (const auto&[k, v] : sections)
                sm << k << v;
        }

    protected:
        std::chrono::steady_clock::time_point managerCreateTime = std::chrono::steady_clock::now();
        std::map<std::string, Section> sections;
    };


    inline SectionManager &sectionManager() {
        static SectionManager sectionManager;
        return sectionManager;
    }


    struct AutoSection {
        AutoSection(std::string name)
                : name{name} {
            sectionManager().start(name);
        }

        ~AutoSection() {
            stop();
        }

        void stop() {
            if (!stopped) {
                sectionManager().stop(name);
                stopped = true;
            }
        }

    protected:
        std::string name;
        bool stopped{false};
    };
};
