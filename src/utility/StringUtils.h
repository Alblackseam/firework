#pragma once
#include <string>


namespace Sexy
{
	template<typename Arg, typename = std::enable_if_t<std::is_arithmetic_v<Arg>>>
	std::string toString(const Arg& arg)
	{
		return std::to_string(arg);
	}


	inline std::string toString(std::string arg)
	{
		return arg;
	}


	inline std::string toString(const char* arg)
	{
		return std::string{ arg };
	}


	template<typename Arg, typename ... Args, typename = std::enable_if_t<(sizeof...(Args) > 0)>>
	std::string toString(Arg&& arg, Args&& ... args)
	{
		return toString(std::forward<Arg>(arg)) + toString(std::forward<Args>(args)...);
	}
}

#define THIS_PLACE Sexy::toString(__FILE__, " : ", __LINE__, " : ")
